final countries = [
  {
    "name": "Algérie",
    "code": "dza"
  },
  {
    "name": "Angola",
    "code": "ago"
  },
  {
    "name": "Bénin",
    "code": "ben"
  },
  {
    "name": "Botswana",
    "code": "bwa"
  },
  // {
  //   "name": "Territoire britannique de l'océan Indien",
  //   "code": "iot"
  // },
  {
    "name": "Burkina Faso",
    "code": "bfa"
  },
  {
    "name": "Burundi",
    "code": "bdi"
  },
  {
    "name": "Cameroun",
    "code": "cmr"
  },
  {
    "name": "Cap Vert",
    "code": "cpv"
  },
  {
    "name": "République centrafricaine",
    "code": "caf"
  },
  {
    "name": "Tchad",
    "code": "tcd"
  },
  {
    "name": "Comores",
    "code": "com"
  },
  {
    "name": "Congo",
    "code": "cog"
  },
  {
    "name": "Congo (Rép. dém.)",
    "code": "cod"
  },
  {
    "name": "Djibouti",
    "code": "dji"
  },
  {
    "name": "Égypte",
    "code": "egy"
  },
  {
    "name": "Guinée-Équatoriale",
    "code": "gnq"
  },
  {
    "name": "Érythrée",
    "code": "eri"
  },
  {
    "name": "Éthiopie",
    "code": "eth"
  },
  {
    "name": "Terres australes et antarctiques françaises",
    "code": "atf"
  },
  {
    "name": "Gabon",
    "code": "gab"
  },
  {
    "name": "Gambie",
    "code": "gmb"
  },
  {
    "name": "Ghana",
    "code": "gha"
  },
  {
    "name": "Guinée",
    "code": "gin"
  },
  {
    "name": "Guinée-Bissau",
    "code": "gnb"
  },
  {
    "name": "Côte d'Ivoire",
    "code": "civ"
  },
  {
    "name": "Kenya",
    "code": "ken"
  },
  {
    "name": "Lesotho",
    "code": "lso"
  },
  {
    "name": "Liberia",
    "code": "lbr"
  },
  {
    "name": "Libye",
    "code": "lby"
  },
  {
    "name": "Madagascar",
    "code": "mdg"
  },
  {
    "name": "Malawi",
    "code": "mwi"
  },
  {
    "name": "Mali",
    "code": "mli"
  },
  {
    "name": "Mauritanie",
    "code": "mrt"
  },
  {
    "name": "Île Maurice",
    "code": "mus"
  },
  {
    "name": "Mayotte",
    "code": "myt"
  },
  {
    "name": "Maroc",
    "code": "mar"
  },
  {
    "name": "Mozambique",
    "code": "moz"
  },
  {
    "name": "Namibie",
    "code": "nam"
  },
  {
    "name": "Niger",
    "code": "ner"
  },
  {
    "name": "Nigéria",
    "code": "nga"
  },
  {
    "name": "Réunion",
    "code": "reu"
  },
  {
    "name": "Rwanda",
    "code": "rwa"
  },
  {
    "name": "Sainte-Hélène",
    "code": "shn"
  },
  {
    "name": "Sao Tomé-et-Principe",
    "code": "stp"
  },
  {
    "name": "Sénégal",
    "code": "sen"
  },
  {
    "name": "Seychelles",
    "code": "syc"
  },
  {
    "name": "Sierra Leone",
    "code": "sle"
  },
  {
    "name": "Somalie",
    "code": "som"
  },
  // {
  //   "name": "Afrique du Sud",
  //   "code": "ads"
  // },
  {
    "name": "Soudan du Sud",
    "code": "ssd"
  },
  {
    "name": "Soudan",
    "code": "sdn"
  },
  {
    "name": "Swaziland",
    "code": "swz"
  },
  {
    "name": "Tanzanie",
    "code": "tza"
  },
  {
    "name": "Togo",
    "code": "tgo"
  },
  {
    "name": "Tunisie",
    "code": "tun"
  },
  {
    "name": "Uganda",
    "code": "uga"
  },
  {
    "name": "Sahara Occidental",
    "code": "esh"
  },
  {
    "name": "Zambie",
    "code": "zmb"
  },
  {
    "name": "Zimbabwe",
    "code": "zwe"
  }
];