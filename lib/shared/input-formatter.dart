import 'package:flutter/services.dart';

class SpacedNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

    String newText = "";
    final String newValueWithoutSpace = newValue.text.replaceAll(RegExp(r"\s*"), "");
    final int newTextLength = newValueWithoutSpace.length;

    for(int i = newTextLength - 1; i >= 0; i--) {
      newText = "${newValueWithoutSpace[i]}$newText";
      if ((newTextLength - i) % 3 == 0) {
        newText = i > 0 ? " $newText" : newText;
      }
    }

    final String text = newText.toString();
    int newValueLength = text.length;

    return TextEditingValue(text: text, selection: TextSelection.collapsed(offset: newValueLength));
  }
}
