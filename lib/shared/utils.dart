import 'package:empire/shared/contries.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

final projectStatus = {
  0: ["Projet crée", Colors.white],
  1: ["En cours de traitement", Colors.orange],
  2: ["Accepté", Colors.green],
  3: ["Clôturé", Colors.blue],
  4: ["Rejeté", Colors.red],
};

String? countryFlag(String country) {
  Map<String, String>? object = countries.singleWhereOrNull((element) => element['name']?.toLowerCase() == country.toLowerCase());
  return object?['code'];
}