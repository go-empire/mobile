import 'package:form_field_validator/form_field_validator.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:validators/validators.dart' as validators;

Map<String, dynamic>? spacedNumber(AbstractControl<dynamic> control) {
  if (control.isNull) return { 'spacedNumber': true };

  final stringValue = control.value.toString().replaceAll(' ', '');
  final intValue = int.tryParse(stringValue);

  return intValue != null ? null : { 'spacedNumber': true };
}

class CustomValidator {
  static Map<String, dynamic>? isDate(AbstractControl<dynamic> control) {
    return control.isNotNull && validators.isDate(control.value) ? null : { 'isDate': true };
  }
}