import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class RepresentantRepository {
  Future<List<User>?> fetch() async {
    final data = await Services.api.get(Const.api.allRepresentants) as List?;
    if (data == null) return null;

    return data.map((user) => User.fromMap(user)).toList(growable: false);
  }
}