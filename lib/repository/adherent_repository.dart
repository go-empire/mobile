import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class AdherentRepository {
  Future<List<Adherent>?> fetch() async {
    final data = await Services.api.get(Const.api.adherents) as List?;
    if (data == null) return null;

    return data.map((adherent) => Adherent.fromMap(adherent)).toList(growable: false);
  }

  Future<Adherent?> create(CreateAdherentDto adherentDto) async {
    final data = await Services.api.post(Const.api.adherents, adherentDto.toMap());
    if (data == null) return null;

    return Adherent.fromMap(data);
  }


  Future<Adherent?> update(String id, CreateAdherentDto adherentDto) async {
    final data = await Services.api.patch("${Const.api.adherents}/$id", adherentDto.toMap());
    if (data == null) return null;

    return Adherent.fromMap(data);
  }

  Future<User?> createRepresentant(CreateRepresentantDto createRepresentantDto) async {
    final result = await Services.api.post(Const.api.users, createRepresentantDto.toMap());
    if (result == null) return null;

    return User.fromMap(result);
  }

  Future<List<User>?> representants(String id) async {
    final data = await Services.api.get("/adherents/$id/representants") as List?;
    if (data == null) return null;

    return data.map((user) => User.fromMap(user)).toList(growable: false);
  }

  Future<Adherent?> addPoints(String adherentId, int points) async {
    final data = await Services.api.post("${Const.api.addPoints}", { 'adherentId': adherentId, 'points': points });
    if (data == null) return null;

    return Adherent.fromMap(data);
  }
}