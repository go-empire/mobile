import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class UserRepository {

  Future<User?> update(String id, dynamic body) async {
    final data = await Services.api.patch("${Const.api.users}/$id", body);
    if (data == null) return null;

    return User.fromMap(data);
  }

  Future<User?> get(String id) async {
    final data = await Services.api.get("${Const.api.users}/$id");
    if (data == null) return null;

    return User.fromMap(data);
  }

  Future<List<User>?> fetch() async {
    final data = await Services.api.get("${Const.api.users}") as List?;
    if (data == null) return null;

    return data.map((user) => User.fromMap(user)).toList();
  }
}