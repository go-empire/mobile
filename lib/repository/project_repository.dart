import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_project_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class ProjectRepository {
  Future<List<Project>?> fetch() async {
    final data = await Services.api.get(Const.api.projects) as List?;
    if (data == null) return null;

    return data.map((project) => Project.fromMap(project)).toList(growable: false);
  }

  Future<Project?> create(CreateProjectDto projectDto) async {
    final data = await Services.api.post(Const.api.projects, projectDto.toMap());
    if (data == null) return null;

    return Project.fromMap(data);
  }

  Future<Project?> update(String id, dynamic data) async {
    final result = await Services.api.patch(Const.api.projects + "/$id", data);
    if (result == null) return null;

    return Project.fromMap(result);
  }
}