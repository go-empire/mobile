import 'package:empire/constants/constants.dart';
import 'package:empire/models/article.dart';
import 'package:empire/services/services.dart';

class ArticleRepository {
  Future<List<Article>?> upload() async {
    final data = await Services.api.get("${Const.api.articles}") as List?;
    if (data == null) return null;

    return data.map((article) => Article.fromJson(article)).toList(growable: false);
  }
}