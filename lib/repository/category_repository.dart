import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_notification_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/category.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class CategoryRepository {
  Future<List<Category>?> fetch() async {
    final data = await Services.api.get(Const.api.categories) as List?;
    if (data == null) return null;

    return data.map((category) => Category.fromMap(category)).toList(growable: false);
  }

  Future<Category?> create(String name) async {
    final data = await Services.api.post(Const.api.categories, { 'name': name });
    if (data == null) return null;

    return Category.fromMap(data);
  }

  Future<Category?> update(String id, String name) async {
    final data = await Services.api.patch(Const.api.categories + "/" + id, { 'name': name });
    if (data == null) return null;

    return Category.fromMap(data);
  }
}