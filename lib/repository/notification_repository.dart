import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_notification_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class NotificationRepository {
  Future<List<Notification>?> fetch() async {
    final data = await Services.api.get(Const.api.notifications) as List?;
    if (data == null) return null;

    return data.map((notification) => Notification.fromMap(notification)).toList(growable: false);
  }

  Future<Notification?> create(CreateNotificationDto createNotificationDto) async {
    final data = await Services.api.post(Const.api.notifications, createNotificationDto.toMap());
    if (data == null) return null;

    return Notification.fromMap(data);
  }

  Future<Notification?> update(CreateNotificationDto createNotificationDto) async {
    final data = await Services.api.patch(Const.api.notifications, createNotificationDto);
    if (data == null) return null;

    return Notification.fromMap(data);
  }

  Future<Notification?> delete(String id) async {
    final data = await Services.api.delete(Const.api.notifications + "/$id");
    if (data == null) return null;

    return Notification.fromMap(data);
  }
}