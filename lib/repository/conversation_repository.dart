import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/conversation.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';

class ConversationRepository {
  Future<List<Conversation>?> fetch() async {
    final data = await Services.api.get(Const.api.conversations) as List?;
    if (data == null) return null;

    return data.map((conversation) => Conversation.fromMap(conversation)).toList(growable: false);
  }

  Future<List<Message>?> fetchMessages(String conversationId) async {
    final data = await Services.api.get(Const.api.chatMessages + "/$conversationId") as List?;
    if (data == null) return null;

    return data.map((message) => Message.fromMap(message)).toList(growable: false);
  }

  Future<Conversation?> userConversation(String userId) async {
    final data = await Services.api.get(Const.api.userConversation + "/$userId" + "/conversation");
    if (data == null) return null;

    return Conversation.fromMap(data);
  }
}