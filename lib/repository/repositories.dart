import 'package:empire/repository/adherent_repository.dart';
import 'package:empire/repository/article_repository.dart';
import 'package:empire/repository/conversation_repository.dart';
import 'package:empire/repository/project_repository.dart';
import 'package:empire/repository/representant_repository.dart';
import 'package:empire/repository/user_repository.dart';

import 'category_repository.dart';
import 'notification_repository.dart';

class Repositories {
  static final adherent = AdherentRepository();
  static final project = ProjectRepository();
  static final article = ArticleRepository();
  static final user = UserRepository();
  static final conversation = ConversationRepository();
  static final notifications = NotificationRepository();
  static final category = CategoryRepository();
  static final representant = RepresentantRepository();
}