class Category {
  String? id;
  String? name;

  Category({
    this.id,
    this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Category.fromMap(dynamic map) {
    return Category(
      id: map['id']?.toString(),
      name: map['name']?.toString(),
    );
  }
}
