class Fichier {
  String id;
  String filename;
  String mimetype;
  int size;
  String createdById;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? deletedAt;
  String owner;
  String name;
  String kind;

  Fichier({
    required this.id,
    required this.filename,
    required this.mimetype,
    required this.size,
    required this.createdById,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    required this.owner,
    required this.name,
    required this.kind,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'filename': this.filename,
      'mimetype': this.mimetype,
      'size': this.size,
      'createdById': this.createdById,
      'createdAt': this.createdAt,
      'updatedAt': this.updatedAt,
      'deletedAt': this.deletedAt,
      'owner': this.owner,
      'name': this.name,
      'kind': this.kind,
    };
  }

  factory Fichier.fromMap(Map<String, dynamic> map) {
    return Fichier(
      id: map['id'] as String,
      filename: map['filename'] as String,
      mimetype: map['mimetype'] as String,
      size: map['size'] as int,
      createdById: map['createdById'] as String,
      createdAt: DateTime.tryParse(map['createdAt'] ?? ""),
      updatedAt: DateTime.tryParse(map['updatedAt'] ?? ""),
      deletedAt: DateTime.tryParse(map['deletedAt'] ?? ""),
      owner: map['owner'] as String,
      name: map['name'] as String,
      kind: map['kind'] as String,
    );
  }

  Fichier copyWith({
    String? id,
    String? filename,
    String? mimetype,
    int? size,
    String? createdById,
    DateTime? createdAt,
    DateTime? updatedAt,
    DateTime? deletedAt,
    String? owner,
    String? name,
    String? kind,
  }) {
    return Fichier(
      id: id ?? this.id,
      filename: filename ?? this.filename,
      mimetype: mimetype ?? this.mimetype,
      size: size ?? this.size,
      createdById: createdById ?? this.createdById,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
      owner: owner ?? this.owner,
      name: name ?? this.name,
      kind: kind ?? this.kind,
    );
  }
}
