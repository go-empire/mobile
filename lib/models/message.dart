class Message {
  String? id;
  String? text;
  String? type;
  String? senderId;
  String? receiverId;
  String? conversationId;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? deletedAt;

  Message({
    this.id,
    this.text,
    this.type,
    this.senderId,
    this.receiverId,
    this.conversationId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Message.fromMap(dynamic map) {
    var temp;
    return Message(
      id: map['id']?.toString(),
      text: map['text']?.toString(),
      type: map['type']?.toString(),
      senderId: map['senderId']?.toString(),
      receiverId: map['receiverId']?.toString(),
      conversationId: map['conversationId']?.toString(),
      createdAt: null == (temp = map['createdAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      updatedAt: null == (temp = map['updatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      deletedAt: null == (temp = map['deletedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
      'type': type,
      'senderId': senderId,
      'receiverId': receiverId,
      'conversationId': conversationId,
      'createdAt': createdAt?.toString(),
      'updatedAt': updatedAt?.toString(),
      'deletedAt': deletedAt?.toString(),
    };
  }
}
