class Conversation {

  String? profileUrl;
  String? firstname;
  String? conversationId;
  String? fullname;
  String? userId;
  String? username;
  String? lastname;

  Conversation({
    this.profileUrl,
    this.firstname,
    this.conversationId,
    this.fullname,
    this.userId,
    this.username,
    this.lastname,
  });

  factory Conversation.fromMap(dynamic map) {
    return Conversation(
      profileUrl: map['profileUrl']?.toString(),
      firstname: map['firstname']?.toString(),
      conversationId: map['conversationId']?.toString(),
      fullname: map['fullname']?.toString(),
      userId: map['userId']?.toString(),
      username: map['username']?.toString(),
      lastname: map['lastname']?.toString(),
    );
  }
}
