// To parse this JSON data, do
//
//     final adherent = adherentFromMap(jsonString);

import 'dart:convert';

class Adherent {
  final String? id;
  final String? name;
  final String? logo;
  final String? logoId;
  final int? revenue;
  final String? phone;
  final String? country;
  final String? level;
  final String? activity;
  final int? points;
  final String? categoryId;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final DateTime? deletedAt;
  final String? email;
  final String? address;
  final int? representants;
  final String? category;


  Adherent copyWith({
    final String? id,
    final String? name,
    final String? logo,
    final String? logoId,
    final int? revenue,
    final String? phone,
    final String? country,
    final String? level,
    final String? activity,
    final int? points,
    final String? categoryId,
    final DateTime? createdAt,
    final DateTime? updatedAt,
    final DateTime? deletedAt,
    final String? email,
    final String? address,
    final int? representants,
    final String? category,
  }) {
    return Adherent(
      id: id ?? this.id,
      name: name ?? this.name,
      logo: logo ?? this.logo,
      logoId: logo ?? this.logoId,
      revenue: revenue ?? this.revenue,
      phone: phone ?? this.phone,
      country: country ?? this.country,
      level: level ?? this.level,
      activity: activity ?? this.activity,
      points: points ?? this.points,
      categoryId: categoryId ?? this.categoryId,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
      email: email ?? this.email,
      address: address ?? this.address,
      representants: representants ?? this.representants,
      category: category ?? this.category,
    );
  }

  factory Adherent.fromMap(dynamic map) {
    var temp;
    return Adherent(
      id: map['id']?.toString(),
      name: map['name']?.toString(),
      logo: map['logo']?.toString(),
      logoId: map['logoId']?.toString(),
      revenue: null == (temp = map['revenue']) ? null : (temp is num ? temp.toInt() : int.tryParse(temp)),
      phone: map['phone']?.toString(),
      country: map['country']?.toString(),
      level: map['level']?.toString(),
      activity: map['activity']?.toString(),
      points: null == (temp = map['points']) ? null : (temp is num ? temp.toInt() : int.tryParse(temp)),
      categoryId: map['categoryId']?.toString(),
      createdAt: null == (temp = map['createdAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      updatedAt: null == (temp = map['updatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      deletedAt: null == (temp = map['deletedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      email: map['email']?.toString(),
      address: map['address']?.toString(),
      representants: null == (temp = map['representants']) ? null : (temp is num ? temp.toInt() : int.tryParse(temp)),
      category: map['category']?.toString(),
    );
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'logo': logo,
      'logoId': logoId,
      'revenue': revenue,
      'phone': phone,
      'country': country,
      'level': level,
      'activity': activity,
      'points': points,
      'categoryId': categoryId,
      'createdAt': createdAt?.toString(),
      'updatedAt': updatedAt?.toString(),
      'deletedAt': deletedAt?.toString(),
      'email': email,
      'address': address,
      'representants': representants,
      'category': category,
    };
  }

  Adherent({
    this.id,
    this.name,
    this.logo,
    this.logoId,
    this.revenue,
    this.phone,
    this.country,
    this.level,
    this.activity,
    this.points,
    this.categoryId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.email,
    this.address,
    this.representants,
    this.category,
  });
}
