import 'dart:convert';

class User {
  final String? id;
  final String? username;
  final String? fullname;
  final String? email;
  final String? firstname;
  final String? lastname;
  final DateTime? birthday;
  final String? profilePictureUrl;
  final String? profilePictureId;
  final String? job;
  final String? phone;
  final bool? isRepresentant;
  final DateTime? datejoined;
  final String? adherentId;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final DateTime? deletedAt;
  final bool? loggedIn;
  final String? roles;

  const User({
    this.id,
    this.username,
    this.fullname,
    this.email,
    this.firstname,
    this.lastname,
    this.birthday,
    this.profilePictureUrl,
    this.profilePictureId,
    this.job,
    this.phone,
    this.isRepresentant,
    this.datejoined,
    this.adherentId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.loggedIn,
    this.roles,
  });

  factory User.fromMap(dynamic map) {
    var temp;
    return User(
      id: map['id']?.toString(),
      username: map['username']?.toString(),
      fullname: map['fullname']?.toString(),
      email: map['email']?.toString(),
      firstname: map['firstname']?.toString(),
      lastname: map['lastname']?.toString(),
      birthday: null == (temp = map['birthday']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      profilePictureUrl: map['profilePictureUrl']?.toString(),
      profilePictureId: map['profilePictureId']?.toString(),
      job: map['job']?.toString(),
      phone: map['phone']?.toString(),
      isRepresentant:
          null == (temp = map['isRepresentant']) ? null : (temp is bool ? temp : (temp is num ? 0 != temp.toInt() : ('true' == temp.toString()))),
      datejoined: null == (temp = map['datejoined']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      adherentId: map['adherentId']?.toString(),
      createdAt: null == (temp = map['createdAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      updatedAt: null == (temp = map['updatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      deletedAt: null == (temp = map['deletedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      loggedIn: null == (temp = map['loggedIn']) ? null : (temp is bool ? temp : (temp is num ? 0 != temp.toInt() : ('true' == temp.toString()))),
      roles: map['roles']?.toString(),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'username': username,
      'fullname': fullname,
      'email': email,
      'firstname': firstname,
      'lastname': lastname,
      'birthday': birthday?.toString(),
      'profilePictureUrl': profilePictureUrl,
      'profilePictureId': profilePictureId,
      'job': job,
      'phone': phone,
      'isRepresentant': isRepresentant,
      'datejoined': datejoined?.toString(),
      'adherentId': adherentId,
      'createdAt': createdAt?.toString(),
      'updatedAt': updatedAt?.toString(),
      'deletedAt': deletedAt?.toString(),
      'loggedIn': loggedIn,
      'roles': roles,
    };
  }
}
