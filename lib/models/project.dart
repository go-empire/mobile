class Project {
  String? id;
  String? title;
  String? description;
  int? budget;
  DateTime? startDate;
  int? duration;
  int? status;
  String? adherentId;
  DateTime? updatedAt;
  DateTime? createdAt;
  DateTime? deletedAt;
  String? imageUrl;

  Project({
    this.id,
    this.title,
    this.description,
    this.budget,
    this.startDate,
    this.duration,
    this.status,
    this.adherentId,
    this.updatedAt,
    this.createdAt,
    this.deletedAt,
    this.imageUrl,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'description': this.description,
      'budget': this.budget,
      'startDate': this.startDate,
      'adherentId': this.adherentId,
      'duration': this.duration,
      'status': this.status,
      'updatedAt': this.updatedAt,
      'createdAt': this.createdAt,
      'deletedAt': this.deletedAt,
      'imageUrl': this.imageUrl,
    };
  }

  factory Project.fromMap(Map<String, dynamic> map) {
    return Project(
      id: map['id'] as String?,
      title: map['title'] as String?,
      description: map['description'] as String?,
      budget: map['budget'] as int?,
      adherentId: map['adherentId'] as String?,
      startDate: DateTime.tryParse(map['startDate'] ?? ""),
      duration: map['duration'] as int?,
      status: map['status'] as int?,
      updatedAt: DateTime.tryParse(map['updatedAt'] ?? ""),
      createdAt: DateTime.tryParse(map['createdAt'] ?? ""),
      deletedAt: DateTime.tryParse(map['deletedAt'] ?? ""),
      imageUrl: map['imageUrl'] as String?,
    );
  }
}
