class Notification {
  String? id;
  String? title;
  String? message;
  String? createdById;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? deletedAt;


  factory Notification.fromMap(dynamic map) {
    var temp;
    return Notification(
      id: map['id']?.toString(),
      title: map['title']?.toString(),
      message: map['message']?.toString(),
      createdById: map['createdById']?.toString(),
      createdAt: null == (temp = map['createdAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      updatedAt: null == (temp = map['updatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      deletedAt: null == (temp = map['deletedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'message': message,
      'createdById': createdById,
      'createdAt': createdAt?.toString(),
      'updatedAt': updatedAt?.toString(),
      'deletedAt': deletedAt?.toString(),
    };
  }

  Notification({
    this.id,
    this.title,
    this.message,
    this.createdById,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });
}