import 'package:flutter/material.dart';

class Auth {
  String id;
  String token;

  Auth({
    required this.id,
    required this.token,
  });

  factory Auth.fromMap(Map<String, dynamic> map) {
    return new Auth(
      id: map['id'] as String,
      token: map['token'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'id': this.id,
      'token': this.token,
    } as Map<String, dynamic>;
  }
}