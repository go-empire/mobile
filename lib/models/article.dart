class Article {
  String? id;
  String? title;
  String? content;
  DateTime? updatedAt;
  DateTime? createdAt;
  String? createdById;
  DateTime? deletedAt;
  String? imageId;
  String? imageUrl;

  Article({this.id, this.title, this.content, this.updatedAt, this.createdAt, this.createdById, this.deletedAt, this.imageId, this.imageUrl});

  Article.fromJson(dynamic json) {
    id = json["id"];
    title = json["title"];
    content = json["content"];
    updatedAt = json["updatedAt"] != null ? DateTime.parse(json["updatedAt"]) : null;
    createdAt = json["createdAt"] != null ? DateTime.parse(json["createdAt"]) : null;
    createdById = json["createdById"];
    deletedAt = json["deletedAt"] != null ? DateTime.parse(json["deletedAt"]) : null;
    imageId = json["imageId"];
    imageUrl = json["imageUrl"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["title"] = title;
    map["content"] = content;
    map["updatedAt"] = updatedAt?.toString();
    map["createdAt"] = createdAt?.toString();
    map["createdById"] = createdById;
    map["deletedAt"] = deletedAt?.toString();
    map["imageId"] = imageId;
    map["imageUrl"] = imageUrl;
    return map;
  }
}
