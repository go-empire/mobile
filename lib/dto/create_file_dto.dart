class CreateFileDto {
  String? owner;
  String kind;
  String name;
  String path;


  CreateFileDto.simple(this.kind, this.name, this.path);

  CreateFileDto({
    this.owner,
    required this.kind,
    required this.name,
    required this.path,
  });


  Map<String, dynamic> toMap() {
    return {
      'owner': this.owner,
      'kind': this.kind,
      'name': this.name,
      'path': this.path,
    };
  }

  factory CreateFileDto.fromMap(Map<String, dynamic> map) {
    return CreateFileDto(
      owner: map['owner'] as String,
      kind: map['kind'] as String,
      name: map['name'] as String,
      path: map['path'] as String,
    );
  }
}