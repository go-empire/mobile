class CreateAdherentDto {
  String? name;
  String? logoId;
  int? revenue;
  String? phone;
  String? country;
  String? level;
  String? activity;
  String? categoryId;
  String? email;
  String? address;

  CreateAdherentDto({
    this.name,
    this.logoId,
    this.revenue,
    this.phone,
    this.country,
    this.level,
    this.activity,
    this.categoryId,
    this.email,
    this.address,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'logoId': this.logoId,
      'revenue': this.revenue,
      'phone': this.phone,
      'country': this.country,
      'level': this.level,
      'activity': this.activity,
      'categoryId': this.categoryId,
      'email': this.email,
      'address': this.address,
    };
  }

  factory CreateAdherentDto.fromMap(Map<String, dynamic> map) {
    return CreateAdherentDto(
      name: map['name'] as String?,
      logoId: map['logoId'] as String?,
      revenue: int.tryParse(map['revenue'].toString().replaceAll(' ', '')),
      phone: map['phone'] as String?,
      country: map['country'] as String?,
      level: map['level'] as String?,
      activity: map['activity'] as String?,
      categoryId: map['categoryId'] as String?,
      email: map['email'] as String?,
      address: map['address'] as String?,
    );
  }


}