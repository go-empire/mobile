class CreateUserDto {
  String? username;
  String? email;
  String? firstname;
  String? lastname;
  String? fullname;
  String? profilePictureUrl;
  String? job;
  String? phone;
  bool? isRepresentant = true;
  String? adherentId;
  bool? loggedIn = false;

}