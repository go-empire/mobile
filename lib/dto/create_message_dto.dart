class CreateMessageDto {
  String? text;
  String? type;
  String? senderId;
  String? receiverId;
  String? conversationId;
  DateTime? createdAt;

  CreateMessageDto({
    this.text,
    this.type,
    this.senderId,
    this.receiverId,
    this.conversationId,
    this.createdAt,
  }) {
    this.createdAt = createdAt ?? DateTime.now();
  }

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'type': type,
      'senderId': senderId,
      'receiverId': receiverId,
      'conversationId': conversationId,
      'createdAt': createdAt.toString(),
    };
  }

  factory CreateMessageDto.fromMap(dynamic map) {
    return CreateMessageDto(
      text: map['text']?.toString(),
      type: map['type']?.toString(),
      senderId: map['senderId']?.toString(),
      receiverId: map['receiverId']?.toString(),
      conversationId: map['conversationId']?.toString(),
      createdAt: DateTime.tryParse(map['createdAt'] ?? DateTime.now().toString()),
    );
  }
}
