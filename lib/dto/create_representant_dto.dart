class CreateRepresentantDto {
  String? username;
  String? email;
  String? firstname;
  String? lastname;
  String? fullname;
  String? profilePictureUrl;
  String? job;
  String? phone;
  bool? isRepresentant = true;
  String? adherentId;
  bool? loggedIn = false;

  CreateRepresentantDto({
    this.username,
    this.email,
    this.firstname,
    this.fullname,
    this.lastname,
    this.profilePictureUrl,
    this.job,
    this.phone,
    this.adherentId,
  });

  Map<String, dynamic> toMap() {
    return {
      'username': this.username,
      'email': this.email,
      'firstname': this.firstname,
      'fullname': this.fullname,
      'lastname': this.lastname,
      'profilePictureUrl': this.profilePictureUrl,
      'job': this.job,
      'phone': this.phone,
      'isRepresentant': this.isRepresentant,
      'adherentId': this.adherentId,
      'loggedIn': this.loggedIn,
    };
  }

  factory CreateRepresentantDto.fromMap(Map<String, dynamic> map) {
    return CreateRepresentantDto(
      username: map['username'] as String?,
      email: map['email'] as String?,
      firstname: map['firstname'] as String?,
      lastname: map['lastname'] as String?,
      fullname: map['fullname'] as String?,
      profilePictureUrl: map['profilePictureUrl'] as String?,
      job: map['job'] as String?,
      phone: map['phone'] as String?,
      adherentId: map['adherentId'] as String?,
    );
  }
}