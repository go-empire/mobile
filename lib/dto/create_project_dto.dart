class CreateProjectDto {
  String title;
  String description;
  int? budget;
  DateTime? startDate;
  int? duration;
  int? status;
  String? imageId;

  CreateProjectDto({
    required this.title,
    required this.description,
    this.budget,
    this.startDate,
    this.duration,
    this.status,
    this.imageId,
  });

  Map<String, dynamic> toMap() {
    return {
      'title': this.title,
      'description': this.description,
      'budget': this.budget,
      'startDate': this.startDate.toString(),
      'duration': this.duration,
      'status': this.status,
      'imageId': this.imageId,
    };
  }

  factory CreateProjectDto.fromMap(Map<String, dynamic> map) {
    return CreateProjectDto(
      title: map['title'] as String,
      description: map['description'] as String,
      budget: int.tryParse(map['budget']),
      startDate: DateTime.tryParse(map['startDate']),
      duration: int.tryParse(map['duration']) ?? 0,
      imageId: map['imageId'] as String?,
      status: int.tryParse(map['status'] ?? "") ?? 0,
    );
  }
}