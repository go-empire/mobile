class CreateNotificationDto {
  String? title;
  String? message;

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'message': message,
    };
  }


  factory CreateNotificationDto.fromMap(dynamic map) {
    return CreateNotificationDto(
      title: map['title']?.toString(),
      message: map['message']?.toString(),
    );
  }
  CreateNotificationDto({
    this.title,
    this.message,
  });
}