import 'dart:io' show Platform;

import 'package:empire/ui/adherent/add_adherent_page.dart';
import 'package:empire/ui/adherent/add_representant_page.dart';
import 'package:empire/ui/adherent/adherent_details.dart';
import 'package:empire/ui/adherent/categories_list.dart';
import 'package:empire/ui/adherent/search_adherents_page.dart';
import 'package:empire/ui/adherent/update_adherent.dart';
import 'package:empire/ui/article/article_detail_page.dart';
import 'package:empire/ui/article/article_list_page.dart';
import 'package:empire/ui/auth/signin_page.dart';
import 'package:empire/ui/chat/add_conversation_page.dart';
import 'package:empire/ui/chat/admin_chat.dart';
import 'package:empire/ui/chat/admin_chat_page.dart';
import 'package:empire/ui/chat/conversations_page.dart';
import 'package:empire/ui/dispatcher.dart';
import 'package:empire/ui/home_page.dart';
import 'package:empire/ui/notification/add_notification_page.dart';
import 'package:empire/ui/notification/notifications_page.dart';
import 'package:empire/ui/points/add_points_page.dart';
import 'package:empire/ui/points/convert_points_page.dart';
import 'package:empire/ui/points/user_points_page.dart';
import 'package:empire/ui/project/add_project_page.dart';
import 'package:empire/ui/project/project_detail_page.dart';
import 'package:empire/ui/project/projects_list_page.dart';
import 'package:empire/ui/project/search_project_page.dart';
import 'package:empire/ui/qrcode/qrcode_page.dart';
import 'package:empire/ui/shared/select_country.dart';
import 'package:empire/ui/adherent/select_star.dart';
import 'package:empire/ui/shared/select_string.dart';
import 'package:empire/ui/users/add_user_page.dart';
import 'package:empire/ui/users/user_details_page.dart';
import 'package:empire/ui/users/profile_page.dart';
import 'package:empire/ui/adherent/add_representant_page.dart';
import 'package:empire/ui/users/update_user_page.dart';
import 'package:empire/ui/users/users_page.dart';
import 'package:flutter/material.dart';

class Routes {
  static const home = '/home';
  static const dashboard = '/dashboard';
  static const adminDashboard = '/admin-dashboard';
  static const signIn = '/signin';
  static const signUp = '/signup';

  static const addAdherent = '/adherents/add';
  static const selectCountry = '/select-country';
  static const selectString = '/select-string';
  static const selectCategory = '/select-category';
  static const selectStar = '/select-star';
  static const adherentDetails = '/adherents/details';
  static const projectList = '/projects';
  static const projectDetail = '/projects/detail';
  static const newProject = '/projects/new';
  static const articleList = '/articles';
  static const articleDetail = '/articles/detail';

  static const profile = '/profile';

  static const qrcode = '/qrcode';

  static const chat = '/chat';

  static const alerts = '/alerts';

  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => Dispatcher());
      case home:
        return MaterialPageRoute(builder: (_) => HomePage());
      case signIn:
        return MaterialPageRoute(builder: (_) => SigninPage());
      case addAdherent:
        return MaterialPageRoute(builder: (_) => AddAdherentPage());
      case selectCountry:
        return MaterialPageRoute(builder: (_) => SelectCountry());
      case selectString:
        return MaterialPageRoute(builder: (_) => SelectString());
      case selectCategory:
        return MaterialPageRoute(builder: (_) => CategoriesList());
      case selectStar:
        return MaterialPageRoute(builder: (_) => SelectStar());
      case adherentDetails:
        return MaterialPageRoute(builder: (_) => AdherentDetails());
      case projectList:
        return MaterialPageRoute(builder: (_) => ProjectListPage());
      case projectDetail:
        return MaterialPageRoute(builder: (_) => ProjectDetailPage());
      case articleList:
        return MaterialPageRoute(builder: (_) => ArticleListPage());
      case articleDetail:
        return MaterialPageRoute(builder: (_) => ArticleDetailPage());
      case ProfilePage.path:
        return MaterialPageRoute(builder: (_) => ProfilePage());
      case newProject:
        return MaterialPageRoute(builder: (_) => AddProjectPage());
      case chat:
        return MaterialPageRoute(builder: (_) => ConversationsPage());
      case qrcode:
        return MaterialPageRoute(builder: (_) => QrcodePage());
      case NotificationPage.path:
        return MaterialPageRoute(builder: (_) => NotificationPage());
      case AddRepresentantPage.path:
        return MaterialPageRoute(builder: (_) => AddRepresentantPage());
      case UpdateAdherent.path:
        return MaterialPageRoute(builder: (_) => UpdateAdherent());
      case AdminChatPage.path:
        return MaterialPageRoute(builder: (_) => AdminChatPage());
      case AdminChat.path:
        return MaterialPageRoute(builder: (_) => AdminChat());
      case AddPointsPage.path:
        return MaterialPageRoute(builder: (_) => AddPointsPage());
      case UserPointsPage.path:
        return MaterialPageRoute(builder: (_) => UserPointsPage());
      case ConvertPointsPage.path:
        return MaterialPageRoute(builder: (_) => ConvertPointsPage());
      case AddNotificationPage.path:
        return MaterialPageRoute(builder: (_) => AddNotificationPage());
      case AddUserPage.path:
        return MaterialPageRoute(builder: (_) => AddUserPage());
      case UpdateUserPage.path:
        return MaterialPageRoute(builder: (_) => UpdateUserPage());
      case UsersPage.path:
        return MaterialPageRoute(builder: (_) => UsersPage());
      case UserDetailsPage.path:
        return MaterialPageRoute(builder: (_) => UserDetailsPage());
      case SearchAdherentPage.path:
        return MaterialPageRoute(builder: (_) => SearchAdherentPage());
      case AddConversationPage.path:
        return MaterialPageRoute(builder: (_) => AddConversationPage());
      case SearchProjectPage.path:
        return MaterialPageRoute(builder: (_) => SearchProjectPage());
      default:
        return MaterialPageRoute(builder: (_) => Dispatcher());
    }
  }
}
