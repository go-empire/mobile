import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/push_service.dart';
import 'package:empire/services/services.dart';
import 'package:empire/ui/auth/signin_page.dart';
import 'package:empire/ui/dispatcher.dart';
import 'package:empire/ui/home_page.dart';
import 'package:empire/ui/onboarding_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'constants/constants.dart';

final authProvider = FutureProvider<Auth?>((ref) {
  return Services.auth.getAuth();
});

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  Services.localNotification.init();
  runApp(ProviderScope(child: MyApp()));
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text("Error initializing Firebase");
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return ScreenUtilInit(
            designSize: Size(375, 667),
            builder: () => GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: MaterialApp(
                title: 'Empire',
                theme: ThemeData(
                    fontFamily: 'Poppins',
                    appBarTheme: AppBarTheme(
                        titleTextStyle: Const.style.pageTitle,
                        iconTheme: IconThemeData(
                          color: Const.color.primary,
                        )),
                    bottomNavigationBarTheme: BottomNavigationBarThemeData(
                      selectedItemColor: Const.color.primary,
                      unselectedItemColor: Const.color.textBody,
                    )),
                onGenerateRoute: Routes.generateRoute,
                home: Consumer(
                  builder: (context, watch, _) {
                    return watch(authProvider).when(
                      data: (auth) {
                        return auth != null ? HomePage() : OnboardingPage();
                      },
                      loading: () => CircularProgressIndicator(),
                      error: (err, stack) {
                        print(err);
                        print(stack);
                        return Center(
                          child: Column(
                            children: [
                              Text("Reessayer"),
                              ElevatedButton(
                                  onPressed: () {
                                    context.refresh(authProvider);
                                  },
                                  child: Text("Reessayer")),
                            ],
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
