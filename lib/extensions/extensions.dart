extension StringUtils on String {
  String get capitalize {
    if (this.isEmpty) return this;

    return this[0].toUpperCase() + this.substring(1);
  }
}

extension IntUtils on int {
  String get human {
    const units = <int, String>{
      1000000000: 'Md',
      1000000: 'M',
      1000: 'K',
    };
    return units.entries
        .map((e) => '${this ~/ e.key}${e.value}')
        .firstWhere((e) => !e.startsWith('0'), orElse: () => '$this');
  }
}

extension Models on Type {
  fromJson(Map<String, dynamic> data) {}
}