import 'package:barcode_scan2/platform_wrapper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrcodePage extends StatelessWidget {
  const QrcodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text("Qrcode", style: Theme.of(context).appBarTheme.titleTextStyle),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pushNamed(context, Routes.newProject);
              },
              child: IconButton(
                icon: Icon(Icons.qr_code_scanner_outlined),
                onPressed: () async {
                  var result = await BarcodeScanner.scan();

                  print(result.type); // The result type (barcode, cancelled, failed)
                  print(result.rawContent); // The barcode content
                  print(result.format); // The barcode format (as enum)
                  print(result.formatNote); // If a unknown f
                },
              ),
            ),
          ],
        ),
        body: SafeArea(child: SingleChildScrollView(
          child: Consumer(
            builder: (context, watch, _) {
              final userState = watch(currentUserNotifier.state);
              if (userState.hasError) {
                return Center(
                  child: Text("Une erreur est survenue"),
                );
              }

              if (userState.isLoading) {
                return Center(
                  child: CircularProgressIndicator(color: Colors.white),
                );
              }

              final user = userState.user;
              return ProviderListener(
                provider: authStateProvider.state,
                onChange: (context, AuthState state) async {
                  if (state.signedOut) {
                    context.read(authStateProvider).initialize();
                    await Navigator.of(context).pushNamed(Routes.signIn);
                  }
                },
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 16),
                      Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                            color: Const.color.lightWhite,
                            borderRadius: BorderRadius.circular(200),
                            boxShadow: [BoxShadow(offset: Offset(0, 4), color: Colors.black.withOpacity(.16), blurRadius: 6)]),
                        child: CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(user?.profilePictureUrl ?? "https://picsum.photos/200"),
                          radius: 40,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text("${user?.firstname} ${user?.lastname}"),
                      ),
                      SizedBox(height: 40),
                      Consumer(
                        builder: (BuildContext context, T Function<T>(ProviderBase<Object?, T>) watch, Widget? child) {
                          final user = watch(currentUserNotifier.state).user;

                          if (user != null) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Flexible(
                                  fit: FlexFit.loose,
                                  child: QrImage(
                                    data: "${user.email}, ${user.firstname} ${user.lastname}",
                                    version: QrVersions.auto,
                                    size: 200.0,
                                  ),
                                ),
                                SizedBox(height: 50),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 20),
                                  child: Text("Faite vous identifier dans un de nos stores agrées grâce à ce Qrcode.", textAlign: TextAlign.center),
                                )
                              ],
                            );
                          } else {
                            return Text("Une erreur est survenue lors la generation du Qrcode");
                          }
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        )));
  }
}
