import 'package:empire/routes/routes.dart';
import 'package:flutter/material.dart';

class OnboardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset("assets/images/onboarding_image.jpg"),
          SizedBox(height: 24),
          Flexible(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(width: 100, child: Image.asset("assets/images/logo.png")),
                  SizedBox(height: 24),
                  Row(
                    children: [
                      Text("Bienvenue sur ", style: TextStyle(fontSize: 30)),
                      Text("Empire", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
                    ],
                  ),
                  Text("Un carrefour d'opportunités ", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300)),
                  Expanded(
                    child: Center(
                      child: SizedBox(
                        width: double.infinity,
                        height: 46,
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.signIn);
                          },
                          style: ElevatedButton.styleFrom(primary: Colors.black),
                          child: Text("Démarrer"),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
