import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/article_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ArticleListPage extends HookWidget {
  const ArticleListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final articles = useProvider(articleListProvider.state).articles;
    return Scaffold(
        appBar: AppBar(
          title: Text("Tous les articles"),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: ListView.separated(
              itemBuilder: (context, index) {
                final article = articles.elementAt(index);
                final articleDate = "${article.createdAt?.day}/${article.createdAt?.month.toString().padLeft(2, '0')}/${article.createdAt?.year}";
                return GestureDetector(
                  onTap: () {
                    context.read(articleSelectedProvider).state = index;
                    Navigator.pushNamed(context, Routes.articleDetail);
                  },
                  child: Container(
                    clipBehavior: Clip.hardEdge,
                    height: 300,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: article.imageUrl != null
                            ? DecorationImage(
                                image: CachedNetworkImageProvider(article.imageUrl!),
                                fit: BoxFit.cover,
                              )
                            : null),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [Color(0xff000000).withOpacity(0.6), Colors.transparent],
                          stops: [0.4, 1],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 25, top: 25, right: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text("$articleDate", style: TextStyle(fontSize: 12, color: Colors.white.withOpacity(.7))),
                            Text("${article.title}", style: TextStyle(fontSize: 24.sp, fontWeight: FontWeight.w900, color: Colors.white), maxLines: 3,),
                            SizedBox(height: 16),
                            Text(
                              "${article.content} ...",
                              style: TextStyle(color: Colors.white),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Expanded(
                              child: Center(
                                child: Text("Lire la suite", style: TextStyle(color: Colors.white))
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) => SizedBox(height: 10),
              itemCount: articles.length),
        ));
  }
}
