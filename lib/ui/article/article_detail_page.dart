import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/states/article_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ArticleDetailPage extends HookWidget {
  const ArticleDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final index = useProvider(articleSelectedProvider).state;
    final article = useProvider(articleListProvider.state).articles.elementAt(index);

    return Scaffold(
        body: CustomScrollView(
      slivers: [
        SliverAppBar(
          expandedHeight: 200,
          iconTheme: IconTheme.of(context).copyWith(color: Colors.white),
          flexibleSpace: FlexibleSpaceBar(
            background: Container(
              decoration: BoxDecoration(
                image: article.imageUrl != null ? DecorationImage(image: CachedNetworkImageProvider(article.imageUrl!), fit: BoxFit.cover) : null,
              ),
              child: Container(
                  decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff000000).withOpacity(0.6), Colors.transparent],
                  stops: [0.4, 1],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
              )),
            ),
            title: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text("${article.title}"),
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text("${article.content}"),
          ),
        ),
      ],
    ));
  }
}
