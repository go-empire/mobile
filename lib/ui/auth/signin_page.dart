import 'dart:async';

import 'package:empire/constants/constants.dart';
import 'package:empire/main.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/shared/cross_button.dart';
import 'package:empire/ui/shared/login_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:responsive_builder/responsive_builder.dart';

final currentPageState = StateProvider((_) => 0);

class SigninPage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    return ProviderListener<AuthState>(
      provider: authStateProvider.state,
      onChange: (context, state) async {
        print(state.authenticated);
        if (state.authenticated!) {
          await Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
        }
      },
      child: Scaffold(
        body: Container(
          width: 1.sw,
          height: 1.sh,
          child: Stack(
            children: [
              OnboardingView(),
              SingleChildScrollView(
                child: FormFields(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FormFields extends HookWidget {
  const FormFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 30.h),
        SizedBox(width: 170.w, child: Image.asset("assets/images/logo_light.png")),
        SizedBox(
          width: 360,
          child: Consumer(
            builder: (context, watch, _) {
              final authState = watch(authStateProvider.state);
              return Column(
                children: [
                  Text("Connexion", style: TextStyle(fontSize: 24.ssp, color: Colors.white, fontWeight: FontWeight.bold)),
                  SizedBox(height: 16),
                  Row(
                    children: [
                      if (authState.shouldCheckEmail!)
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(100),
                                boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
                            child: TextField(
                                controller: authState.email,
                                textAlignVertical: TextAlignVertical.center,
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.send,
                                style: Const.style.bodyText.copyWith(color: Const.color.darker),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                  icon: Icon(FeatherIcons.mail, color: Const.color.darker),
                                  hintText: "Email",
                                )),
                          ),
                        ),
                      SizedBox(
                        width: 10,
                      ),
                      if (!authState.shouldCheckEmail! && authState.shouldResetPassword!)
                        Flexible(
                          fit: FlexFit.tight,
                          child: Text(
                            "Veuillez modifier votre mot de passe.",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      if (authState.shouldCheckEmail!)
                        FloatingActionButton(
                          onPressed: () async {
                            Services.work.start();
                            await context.read(authStateProvider).checkEmail();
                            Services.work.stop();
                          },
                          child: Consumer(builder: (context, watch, _) {
                            return watch(workProvider).maybeWhen(
                                data: (Task task) {
                                  if (task.working) {
                                    return SizedBox(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 2,
                                      ),
                                    );
                                  }
                                  return Icon(Icons.chevron_right);
                                },
                                orElse: () => Icon(Icons.chevron_right));
                          }),
                          backgroundColor: Const.color.primary,
                        ),
                    ],
                  ),
                  if (!authState.shouldCheckEmail! && !authState.shouldResetPassword!) SigninFields(),
                  if (!authState.shouldCheckEmail! && authState.shouldResetPassword!) ResetPasswordFields(),
                ],
              );
            },
          ),
        ),
        SizedBox(height: 30.h),
        Container(
          child: Consumer(
            builder: (context, watch, _) {
              final currentPage = watch(currentPageState).state;
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(currentPage == 0 ? Icons.radio_button_checked_rounded : Icons.radio_button_unchecked, color: Colors.white),
                  Icon(currentPage == 1 ? Icons.radio_button_checked_rounded : Icons.radio_button_unchecked, color: Colors.white),
                  Icon(currentPage == 2 ? Icons.radio_button_checked_rounded : Icons.radio_button_unchecked, color: Colors.white),
                ],
              );
            },
          ),
        ),
        SizedBox(height: 38),
        SizedBox(
          width: 265,
          child: Text(
            "Lorem Ipsum is simply dummy text of the printing and "
            "typesetting industry. Lorem Ipsum has been the industry'"
            "s standard dummy text ever since the 1500s, when an unknown "
            "a type specimen book",
            style: TextStyle(fontSize: 11, color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 30),
        Text("EMPIRE@2021", style: TextStyle(color: Colors.white))
        // ConstrainedBox(
        //   constraints: BoxConstraints(
        //       maxWidth: getValueForScreenType(
        //     context: context,
        //     mobile: 300.w,
        //     tablet: 200.w,
        //     desktop: 200.w,
        //   )),
        //   child: Column(
        //     children: [
        //       TextField(
        //           controller: emailController,
        //           style: Const.style.bodyText,
        //           decoration: InputDecoration(
        //             icon: Icon(FeatherIcons.mail),
        //             labelText: 'Email',
        //           )),
        //       SizedBox(height: 16),
        //       TextField(
        //           controller: passwordController,
        //           obscureText: true,
        //           style: Const.style.bodyText,
        //           decoration: InputDecoration(
        //             icon: Icon(FeatherIcons.lock),
        //             labelText: 'Mot de passe',
        //           )),
        //     ],
        //   ),
        // ),
        // CrossButton(
        //   "Connexion",
        //   onPress: () async {
        //     Services.work.start();
        //     final email = emailController.text.trim().toLowerCase();
        //     final password = passwordController.text.trim().toLowerCase();
        //     final auth = await Services.auth.signIn(email, password);
        //     if (auth != null) {
        //       await Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
        //     }
        //     Services.work.start();
        //   },
        // ),
      ],
    );
  }
}

class SigninFields extends HookWidget {
  const SigninFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final passwordFocusNode = FocusNode();

    return ProviderListener(
      provider: authStateProvider,
      onChange: (context, provider) {
        print(provider);
      },
      child: Consumer(builder: (context, watch, _) {
        final authState = watch(authStateProvider.state);
        return Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
              child: TextField(
                  controller: authState.email,
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.send,
                  textAlignVertical: TextAlignVertical.center,
                  style: Const.style.bodyText.copyWith(color: Const.color.darker),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    icon: Icon(FeatherIcons.mail, color: Const.color.darker),
                    hintText: "Email",
                  )),
            ),
            SizedBox(height: 16.h),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
              child: TextField(
                focusNode: passwordFocusNode..requestFocus(),
                controller: authState.password,
                obscureText: true,
                style: Const.style.bodyText.copyWith(color: Const.color.darker),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  icon: Icon(FeatherIcons.lock, color: Const.color.darker),
                  hintText: 'Mot de passe',
                ),
              ),
            ),
            SizedBox(height: 16.h),
            LoginButton(
                onPress: () async {
                  Services.work.start();
                  context.read(authStateProvider).signIn();
                  Services.work.stop();
                },
                text: "Connexion",
                primary: true),
          ],
        );
      }),
    );
  }
}

class ResetPasswordFields extends HookWidget {
  const ResetPasswordFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authState = useProvider(authStateProvider.state);

    return Column(
      children: [
        SizedBox(height: 16.h),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
          child: TextField(
              controller: authState.defaultPassword,
              obscureText: true,
              style: Const.style.bodyText.copyWith(color: Const.color.darker),
              decoration: InputDecoration(
                border: InputBorder.none,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                icon: Icon(FeatherIcons.lock, color: Const.color.darker),
                hintText: 'Mot de passe actuel',
              )),
        ),
        SizedBox(height: 16.h),
        AnimatedSize(
          duration: Duration(milliseconds: 600),
          curve: Curves.easeInCubic,
          vsync: useSingleTickerProvider(),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(100),
                boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
            child: TextField(
                controller: authState.password,
                obscureText: true,
                style: Const.style.bodyText.copyWith(color: Const.color.darker),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  icon: Icon(FeatherIcons.lock, color: Const.color.darker),
                  hintText: 'Nouveau mot de passe',
                )),
          ),
        ),
        SizedBox(height: 16.h),
        AnimatedSize(
          duration: Duration(milliseconds: 600),
          curve: Curves.easeInCubic,
          vsync: useSingleTickerProvider(),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(100),
                boxShadow: [BoxShadow(offset: Offset(0, 3), color: Colors.black.withOpacity(.05), blurRadius: 6)]),
            child: TextField(
                controller: authState.confirmPassword,
                obscureText: true,
                style: Const.style.bodyText.copyWith(color: Const.color.darker),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  icon: Icon(FeatherIcons.checkCircle, color: Const.color.darker),
                  hintText: 'Confirmation',
                )),
          ),
        ),
        SizedBox(height: 16.h),
        LoginButton(
            onPress: () async {
              Services.work.start();
              await context.read(authStateProvider).resetPassword();
              Services.work.stop();
            },
            text: "Changer le mot de passe",
            primary: true),
      ],
    );
  }
}

class OnboardingView extends StatefulWidget {
  const OnboardingView({Key? key}) : super(key: key);

  @override
  _OnboardingViewState createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  final controller = PageController(keepPage: true);
  late Timer timer;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      timer = Timer.periodic(Duration(seconds: 7), (timer) async {
        if (mounted) {
          if (controller.page == 2) {
            await controller.animateToPage(0, duration: Duration(milliseconds: 400), curve: Curves.easeInCubic);
            context.read(currentPageState).state = controller.page!.toInt();
            return;
          }
          await controller.nextPage(duration: Duration(milliseconds: 400), curve: Curves.easeInCubic);
          context.read(currentPageState).state = controller.page!.toInt();
        }
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      children: [
        Image.asset("assets/images/onboarding_image_1.png", fit: BoxFit.cover),
        Image.asset("assets/images/onboarding_image_2.png", fit: BoxFit.cover),
        Image.asset("assets/images/onboarding_image_3.png", fit: BoxFit.cover),
      ],
    );
  }
}
