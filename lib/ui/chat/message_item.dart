import 'package:empire/constants/constants.dart';
import 'package:empire/models/message.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class MessageItem extends StatelessWidget {

  final Message message;

  const MessageItem(this.message, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final format = DateFormat('dd MMMM yyyy');
    return Consumer(
      builder: (context, watch, _) {
        User user = watch(currentUserNotifier.state).user as User;

        return Padding(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Column(
            children: [
              Builder(
                builder: (context) {
                  final currentDate = format.format(message.createdAt!);

                  if (lastDate == null || lastDate!.compareTo(currentDate) != 0) {
                    lastDate = currentDate;
                    return Padding(
                      padding: const EdgeInsets.only(top: 48, bottom: 16),
                      child: Text("$currentDate", style: TextStyle(color: Colors.grey)),
                    );
                  }

                  return SizedBox.shrink();
                },
              ),
              message.senderId == user.id ? MyMessage(message) : OtherMessage(message)
            ],
          ),
        );
      }
    );
  }
}

class MyMessage extends StatelessWidget {
  final Message message;
  const MyMessage(this.message, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Flexible(
          child: Container(
            margin: const EdgeInsets.only(right: 16, left: 48),
            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Const.color.blue,
            ),
            child: Text("${message.text}", style: TextStyle(color: Colors.white)),
          ),
        ),
      ],
    );
  }
}

class OtherMessage extends StatelessWidget {
  final Message message;
  const OtherMessage(this.message, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Flexible(
          child: Container(
            margin: const EdgeInsets.only(left: 16, right: 48),
            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Const.color.primary,
            ),
            child: Text("${message.text}", style: TextStyle(color: Colors.white)),
          ),
        ),
      ],
    );
  }
}


