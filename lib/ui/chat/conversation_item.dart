import 'package:empire/constants/constants.dart';
import 'package:empire/models/conversation.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ConversationItem extends StatelessWidget {

  late final Conversation conversation;

  ConversationItem(Conversation conversation, {Key? key}) : super(key: key) {
    this.conversation = conversation;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, _) {
        return Container(
            child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 16),
              child: CircleAvatar(
                radius: 40,
                backgroundColor: Const.color.primary,
                child: CircleAvatar(
                  backgroundImage: conversation.profileUrl != null ? NetworkImage(conversation.profileUrl!) : null,
                  radius: 36,
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${conversation.firstname != null ? conversation.firstname! + " " + conversation.lastname! : conversation.fullname}"),
                ],
              ),
            ),
          ],
        ));
      },
    );
  }
}
