import 'package:empire/constants/constants.dart';
import 'package:empire/states/chat_state.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'conversation_list.dart';

class AdminChatPage extends StatefulWidget {

  static const path = "/chat/admin";

  const AdminChatPage({Key? key}) : super(key: key);

  @override
  State<AdminChatPage> createState() => _AdminChatPageState();
}

class _AdminChatPageState extends State<AdminChatPage> {

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      FirebaseMessaging messaging = FirebaseMessaging.instance;

      NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        badge: true,
        sound: true,
      );

      print('User granted permission: ${settings.authorizationStatus}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        final conversation = watch(conversationSelectedProvider).state;
        return SafeArea(
          top: false,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: Row(
                children: [
                  CircleAvatar(
                    backgroundImage: conversation?.profileUrl != null ? NetworkImage(conversation!.profileUrl!) : null,
                  ),
                  SizedBox(width: 16),
                  Text("${conversation?.fullname}", style: Theme.of(context).appBarTheme.titleTextStyle),
                ],
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 12),
                          height: 40,
                          decoration: BoxDecoration(
                            color: Const.color.primary,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Text("Rechercher", style: Const.style.bodyText.copyWith(color: Const.color.lightWhite, fontWeight: FontWeight.bold)),
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      FloatingActionButton(
                        mini: true,
                        backgroundColor: Const.color.primary,
                        elevation: 2,
                        child: Icon(Icons.tune),
                        onPressed: () {},
                      )
                    ],
                  ),
                  SizedBox(height: 32),
                  ConversationList()
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
