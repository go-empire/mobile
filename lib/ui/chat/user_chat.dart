import 'package:empire/dto/create_message_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:empire/ui/chat/message_item.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'chat_footer.dart';

class UserChat extends StatefulWidget {

  const UserChat();

  @override
  State<UserChat> createState() => _UserChatState();
}

class _UserChatState extends State<UserChat> {


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      FirebaseMessaging messaging = FirebaseMessaging.instance;

      NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );

      print('User granted permission: ${settings.authorizationStatus}');
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text("Chat", style: Theme.of(context).appBarTheme.titleTextStyle),
        ),
      body: Consumer(
        builder: (BuildContext context, watch, Widget? child) {
          Conversation? conversation = context.read(conversationSelectedProvider).state;


          if (conversation == null) {
            return Text("Une erreur est survenue");
          }

          final chatState = watch(chatProvider(conversation.conversationId!).state);
          return Column(
            children: [
              Expanded(
                child: Builder(
                  builder: (context) {
                    if (chatState.isLoading) {
                      return Center(child: CircularProgressIndicator());
                    }

                    final messages = chatState.messages;
                    return ListView.separated(
                        shrinkWrap: true,
                        reverse: true,
                        itemBuilder: (context, index) {
                          final message = messages.elementAt(index);
                          return MessageItem(message);
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 16);
                        },
                        itemCount: messages.length);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: ChatFooter(),
              )
            ],
          );
        },
      )
    );
  }
}
