import 'package:empire/states/states.dart';
import 'package:empire/ui/chat/admin_conversations.dart';
import 'package:empire/ui/chat/user_chat.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ConversationsPage extends StatelessWidget {
  const ConversationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        final user = watch(currentUserNotifier.state).user;
        return user?.isRepresentant == true ? const UserChat() : const AdminConversations();
      },
    );
  }
}
