import 'package:empire/constants/constants.dart';
import 'package:empire/states/representant_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:empire/ui/chat/conversation_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'add_conversation_page.dart';

class AdminConversations extends StatelessWidget {
  const AdminConversations();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text("Chat", style: Theme.of(context).appBarTheme.titleTextStyle),
        ),
        body: SafeArea(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              SizedBox(height: 32),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () async {
                        // research users
                        await Navigator.pushNamed(context, AddConversationPage.path);
                        context.read(searchRepresentantTermProvider).state = "";
                      },
                      child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 12),
                        height: 40,
                        decoration: BoxDecoration(
                          color: Const.color.primary,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Text("Rechercher un représentant", style: Const.style.bodyText.copyWith(color: Const.color.lightWhite)),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 32),
              ConversationList()
            ],
          ),
        )));
  }
}
