import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_message_dto.dart';
import 'package:empire/models/message.dart';
import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/representant_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';

import 'admin_chat.dart';
import 'chat_footer.dart';
import 'conversation_list.dart';
import 'message_item.dart';

class AddConversationPage extends StatelessWidget {
  static const path = "/conversations/add";

  const AddConversationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.color.lightWhite,
        elevation: 0,
        centerTitle: true,
        title: Text("Ajouter une conversation", style: Const.style.pageTitle.copyWith(color: Const.color.primary)),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 15),
          child: Column(
            children: [
              TextField(
                autofocus: true,
                onChanged: (String text) {
                  context.read(searchRepresentantTermProvider).state = text;
                },
                decoration: InputDecoration(filled: true, border: OutlineInputBorder()),
              ),
              SizedBox(height: 25),
              Consumer(builder: (context, watch, _) {
                final representants = watch(searchRepresentantsProvider).state;
                return ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    User representant = representants.elementAt(index);
                    return GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () async {
                        final conversations = context.read(conversationListProvider.state).conversations;
                        Conversation? conversation = conversations.singleWhereOrNull((conversation) => conversation.userId == representant.id);

                        if (conversation == null) {
                          Conversation? conversation = await Repositories.conversation.userConversation(representant.id!);
                          if (conversation != null) {
                            context.read(conversationListProvider).addConversation(conversation);
                            context.read(conversationIdSelectedProvider).state = conversation.conversationId;
                            await Navigator.pushReplacementNamed(context, AdminChat.path);
                          } else {
                            Services.message.displayMessage("Une erreur est survenue lors de l'ouverture du chat, reéssayer plutard");
                          }
                        } else {
                          context.read(conversationIdSelectedProvider).state = conversation.conversationId;
                          await Navigator.pushReplacementNamed(context, AdminChat.path);
                        }
                      },
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 16),
                            child: CircleAvatar(
                              radius: 40,
                              backgroundColor: Const.color.primary,
                              child: CircleAvatar(
                                backgroundImage: representant.profilePictureUrl != null ? NetworkImage(representant.profilePictureUrl ?? "") : null,
                                radius: 36,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${representant.fullname}")
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 20);
                  },
                  itemCount: representants.length,
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
