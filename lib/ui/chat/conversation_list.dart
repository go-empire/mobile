import 'package:empire/states/chat_state.dart';
import 'package:empire/ui/chat/admin_chat_page.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'admin_chat.dart';
import 'conversation_item.dart';

class ConversationList extends StatelessWidget {
  const ConversationList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        final conversationsState = watch(conversationListProvider.state);

        if (conversationsState.isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (conversationsState.hasError) {
          return Text("Error occured");
        }

        return ListView.separated(
            shrinkWrap: true,
            itemBuilder: (context, int index) {
              final conversation = conversationsState.conversations.elementAt(index);
              return GestureDetector(
                onTap: () async {
                  context.read(conversationIdSelectedProvider).state = conversation.conversationId;
                  await Navigator.pushNamed(context, AdminChat.path);
                  context.read(conversationIdSelectedProvider).state = null;
                },
                child: ConversationItem(conversation),
              );
            },
            separatorBuilder: (context, index) {
              return SizedBox(height: 10);
            },
            itemCount: conversationsState.conversations.length);
      },
    );
  }
}
