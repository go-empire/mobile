import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_message_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ChatFooter extends StatefulWidget {

  const ChatFooter({Key? key}) : super(key: key);

  @override
  State<ChatFooter> createState() => _ChatFooterState();
}

class _ChatFooterState extends State<ChatFooter> {

  final controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Conversation? conversation = context.read(conversationSelectedProvider).state;
    controller.text = context.read(savedTextFieldText(conversation?.conversationId ?? "")).state;
  }


  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
      child: Consumer(
        builder: (context, watch, _) {
          User user = context.read(currentUserNotifier.state).user as User;
          Conversation? conversation = context.read(conversationSelectedProvider).state;


          if (conversation == null) {
            return Text("Une erreur est survenue");
          }

          final chatState = watch(chatProvider(conversation.conversationId!).state);
          return Row(
            children: [
              Expanded(
                child: TextField(
                  autofocus: true,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.send,
                  controller: controller,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(200)),
                  ),
                  onChanged: (String text) {
                    context.read(savedTextFieldText(conversation.conversationId ?? "")).state = controller.text;
                  },
                  onSubmitted: (String text) {
                    sendMessage(user.id!, conversation.userId, conversation.conversationId!);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Ink(
                  decoration: ShapeDecoration(
                    color: Const.color.primary,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {
                      sendMessage(user.id!, conversation.userId, conversation.conversationId!);
                    },
                    icon: Icon(FeatherIcons.send),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  sendMessage(String currentUserId, String? otherUserId, String conversationId) {
    final message = CreateMessageDto(
      text: controller.text,
      type: 'text',
      senderId: currentUserId,
      receiverId: isRepresentant(currentUserId, otherUserId) ? null : otherUserId,
      conversationId: conversationId,
    );
    context.read(chatProvider(conversationId)).sendMessage(message);
    context.read(savedTextFieldText(conversationId)).state = "";
    controller.text = "";
  }

  isRepresentant(String currentUserId, String? otherUserId) {
    return otherUserId == currentUserId;
  }
}
