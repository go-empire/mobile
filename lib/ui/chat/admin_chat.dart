import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_message_dto.dart';
import 'package:empire/models/message.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import 'chat_footer.dart';
import 'conversation_list.dart';
import 'message_item.dart';

class AdminChat extends StatelessWidget {
  static const path = "/chat/admin/conversation";

  const AdminChat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final format = DateFormat('dd MMMM yyyy');
    final controller = TextEditingController();
    String? lastDate;
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        User user = watch(currentUserNotifier.state).user as User;
        Conversation? conversation = watch(conversationSelectedProvider).state;


        if (conversation == null) {
          return Text("Une erreur est survenue");
        }

        final chatState = watch(chatProvider(conversation.conversationId!).state);
        return SafeArea(
          top: false,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: Row(
                children: [
                  CircleAvatar(
                    backgroundImage: conversation.profileUrl != null ? NetworkImage(conversation.profileUrl!) : null,
                  ),
                  SizedBox(width: 16),
                  Text("${conversation.fullname}", style: Theme.of(context).appBarTheme.titleTextStyle),
                ],
              ),
            ),
            body: Column(
              children: [
                Expanded(
                  child: Builder(
                    builder: (context) {
                      if (chatState.isLoading) {
                        return Center(child: CircularProgressIndicator());
                      }

                      if (chatState.hasError) {
                        return Center(child: Text("Une erreur est survenue"));
                      }

                      final messages = chatState.messages;
                      return ListView.separated(
                          shrinkWrap: true,
                          reverse: true,
                          itemBuilder: (context, index) {
                            final message = messages.elementAt(index);
                            return MessageItem(message);
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox(height: 16);
                          },
                          itemCount: messages.length);
                    },
                  ),
                ),
                ChatFooter()
              ],
            ),
          ),
        );
      },
    );
  }
}
