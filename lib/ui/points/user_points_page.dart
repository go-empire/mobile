import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/size_extension.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'convert_points_page.dart';

class UserPointsPage extends StatelessWidget {
  static const path = "/points/adherent";

  const UserPointsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(text: '');
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Mes points"),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Consumer(
          builder: (BuildContext context, watch, Widget? child) {
            Adherent? adherent = watch(currentUserAdherentProvider).state;

            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 60.h),
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(text: '${adherent?.points ?? 000 }', style: TextStyle(fontSize: 120, color: Const.color.primary, fontWeight: FontWeight.bold)),
                      TextSpan(text: (adherent?.points ?? 0) > 1 ? 'pts' : 'pt', style: TextStyle(fontSize: 12, color: Const.color.primary, fontWeight: FontWeight.bold)),
                    ]),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, ConvertPointsPage.path);
                    }, child: Text("Convertir"),
                  ),
                  SizedBox(height: 32.h),
                  Text("Historique de mes transactions vide")
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
