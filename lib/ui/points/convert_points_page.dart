import 'dart:math';

import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/size_extension.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ConvertPointsPage extends StatelessWidget {
  static const path = "/points/convert";

  const ConvertPointsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(text: '');
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Convertir mes points"),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Consumer(
          builder: (BuildContext context, watch, Widget? child) {
            Adherent? adherent = watch(currentUserAdherentProvider).state;
            final gifts = ["Voayage gratuit", "Séjour hôtel 5 étoiles"];
            final rand = Random();

            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: SizedBox(
                width: double.infinity,
                child: Wrap(
                  alignment: WrapAlignment.spaceEvenly,
                  children: List.generate(2, (index) {
                    return Container(
                      child: Column(
                        children: [
                          Icon(Icons.card_giftcard_outlined, color: Const.color.blue, size: 100),
                          Text("${gifts.elementAt(index)}"),
                          Text("${400 + rand.nextInt(2000)} pts", style: TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    );
                  }),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
