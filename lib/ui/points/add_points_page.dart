import 'package:empire/constants/constants.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/size_extension.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddPointsPage extends StatelessWidget {
  static const path = "/points/add";

  const AddPointsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(text: '');
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Envoyer des points"),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Column(
          children: [
            SizedBox(height: 60.h),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Text(
                "Veuillez saisir le nombre de points à envoyer",
                style: Const.style.mediumText,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 32.h),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextFormField(
                controller: controller,
                style: TextStyle(fontSize: 110),
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: "0",
                  border: OutlineInputBorder(),
                ),
              ),
            )
          ],
        ),
        bottomNavigationBar: CrossBottomButton(
          "Envoyer les points",
          onPress: () async {
            Services.work.start();
            final points = int.tryParse(controller.text);
            if (points != null) {
              final adherent = context.read(adherentSelectedProvider).state;
              await context.read(adherentProvider).addPoints(adherent.id!, points);
              Services.work.stop();
              Services.message.displayMessage("Les points ont été envoyés à l'adhérent");
              Navigator.pop(context);
            } else {
              Services.message.displayError("Valueur invalide");
              Services.work.stop();
            }
          },
        ),
      ),
    );
  }
}
