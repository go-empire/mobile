import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback onPress;
  final String text;
  final IconData? icon;
  final bool primary;

  const LoginButton({Key? key, required this.onPress, required this.text, this.icon, this.primary = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPress,
        child: Container(
          height: 64,
          width: 360,
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
            color: primary ? Const.color.primary : Const.color.lightWhite,
            borderRadius: BorderRadius.circular(40),
          ),
          child: Container(
            decoration: BoxDecoration(
              color: primary ? Const.color.primary : Const.color.lightWhite,
              borderRadius: BorderRadius.circular(40),
              border: Border.all(color: primary ? Colors.white : Colors.black),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Content(text: text, primary: primary),
                ),
                if (icon != null) Icon(icon),
              ],
            ),
          ),
        ));
  }
}

class Content extends HookWidget {
  const Content({
    Key? key,
    required this.text,
    required this.primary,
  }) : super(key: key);

  final String text;
  final bool primary;

  @override
  Widget build(BuildContext context) {
    final workState = useProvider(workProvider);

    return workState.maybeWhen(data: (Task task) {
      if (task.working) {
        return Center(
          child: CircularProgressIndicator(color: Colors.white),
        );
      }

      return Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(color: primary ? Colors.white : Const.color.primary, fontWeight: FontWeight.bold),
      );
    }, orElse: () {
      return Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(color: primary ? Colors.white : Const.color.primary, fontWeight: FontWeight.bold),
      );
    });
  }
}
