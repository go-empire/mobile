import 'package:empire/constants/constants.dart';
import 'package:empire/shared/contries.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SelectCountry extends StatefulWidget {
  @override
  _SelectCountryState createState() => _SelectCountryState();
}

class _SelectCountryState extends State<SelectCountry> {
  var countrySelected;
  int countrySelectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: () {
          Navigator.pop(context, countrySelected);
        }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text("Sélectionner un pays".toUpperCase()),
      ),
      body: ListView.builder(
        itemCount: countries.length,
        itemBuilder: (context, int index) {
          return ListTile(
            selected: countrySelectedIndex == index,
            selectedTileColor: Colors.white,
            onTap: () {
              setState(() {
                countrySelected = countries.elementAt(index);
                countrySelectedIndex = index;
              });
            },
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(200),
              child: SvgPicture.asset(
                "assets/images/countries/${countries.elementAt(index)["code"]}.svg",
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
            ),
            title: Text("${countries.elementAt(index)["name"]}", style: Const.style.bodyText),
            trailing: countrySelectedIndex == index ? Icon(FeatherIcons.checkCircle, color: Const.color.green) : SizedBox.shrink(),
          );
        },
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          Navigator.pop(context, countrySelected);
        },
        child: Container(
          color: Colors.blue,
          height: 51,
          child: Center(child: Text("Valider", style: Const.style.mediumWhite.copyWith(color: Colors.white))),
        ),
      ),
    );
  }
}
