import 'package:empire/constants/constants.dart';
import 'package:empire/shared/input-formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:reactive_forms/reactive_forms.dart';

class BasicFormField extends StatelessWidget {
  final IconData icon;
  final String prefix;
  TextEditingController? controller;
  final hideLine;
  final TextInputType inputType;
  final List<TextInputFormatter>? inputFormatters;
  final GlobalKey<FormState>? formKey;
  final bool? pickImage;
  String? control;

  BasicFormField(
    this.icon,
    this.prefix,
    this.control, {
    Key? key,
    this.hideLine = false,
    this.inputType = TextInputType.text,
    this.inputFormatters,
    this.pickImage,
    this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: Const.color.primary)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Row(
              children: [
                Icon(icon, color: Const.color.primary),
                SizedBox(width: 16),
                Text(prefix, style: Const.style.thinBodyText.copyWith(color: Const.color.primary, fontWeight: FontWeight.normal)),
                if (inputType == TextInputType.datetime)
                  Expanded(
                    child: ReactiveTextField(
                      onTap: () async {
                        final date = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1900),
                          lastDate: DateTime(2100),
                        );
                        final form = ReactiveForm.of(context) as FormGroup?;
                        form?.control('startDate').value = date.toString().substring(0, 10);
                      },
                      formControlName: control,
                      keyboardType: inputType,
                      inputFormatters: inputFormatters,
                      decoration: InputDecoration(
                        errorMaxLines: 1,
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(vertical: 16),
                      ),
                      style: Const.style.mediumBodyText.copyWith(color: Const.color.primary),
                      textAlign: TextAlign.end,
                    ),
                  ),
                if (inputType != TextInputType.datetime)
                  Expanded(
                    child: ReactiveTextField(
                      onTap: () {
                        // formKey?.currentState?.reassemble();
                      },
                      formControlName: control,
                      keyboardType: inputType,
                      inputFormatters: inputFormatters,
                      showErrors: (_) => false,
                      decoration: InputDecoration(
                        errorMaxLines: 1,
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(vertical: 16),
                      ),
                      style: Const.style.mediumBodyText.copyWith(color: Const.color.primary),
                      textAlign: TextAlign.end,
                    ),
                  ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 75),
            child: Container(
              height: hideLine ? 0 : 1,
              color: Const.color.textBody.withOpacity(.40),
            ),
          )
        ],
      ),
    );
  }
}
