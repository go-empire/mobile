import 'package:empire/constants/constants.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AttachmentCell extends StatefulWidget {
  final ValueNotifier<AttachmentState> state;
  final int index;
  final hideLine;

  const AttachmentCell({
    required this.state,
    required this.index,
    this.hideLine = false,
    Key? key,
  }) : super(key: key);

  @override
  _AttachmentCellState createState() => _AttachmentCellState();
}

class _AttachmentCellState extends State<AttachmentCell> {
  @override
  Widget build(BuildContext context) {
    final attachment = widget.state.value.attachments.elementAt(widget.index);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Icon(FeatherIcons.fileText),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 24, right: 8),
                        child: Text(attachment.filename, overflow: TextOverflow.fade, style: Const.style.mediumBodyText),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12),
                child: Row(
                  children: [
                    SizedBox(
                      child: FloatingActionButton(
                        heroTag: "adherent-attachment-edit-${widget.index}",
                        onPressed: () async {
                          final result = await FilePicker.platform.pickFiles(
                            type: FileType.custom,
                            allowedExtensions: ["pdf", "doc", "docx", "jpg", "png"],
                          );
                          if (result != null) {
                            final controller = widget.state.value;
                            final attachments = controller.attachments;
                            final attachment = attachments.elementAt(widget.index);
                            attachment.filePath = result.files.elementAt(0).path!;
                            attachment.filename = result.files.elementAt(0).name;
                            widget.state.value = widget.state.value.copyWith(attachments: attachments);
                          }
                        },
                        child: Icon(FeatherIcons.edit3, color: Colors.white),
                        backgroundColor: Const.color.primary,
                      ),
                    ),
                    SizedBox(width: 16),
                    FloatingActionButton(
                      heroTag: "adherent-attachment-delete-${widget.index}",
                      onPressed: () {
                        widget.state.value.attachments.removeAt(widget.index);
                        widget.state.value = widget.state.value.copyWith(attachments: widget.state.value.attachments);
                      },
                      child: Icon(
                        FeatherIcons.trash2,
                        color: Colors.white,
                      ),
                      backgroundColor: Const.color.red,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 75),
          height: widget.hideLine ? 0 : 1,
          color: Const.color.textBody.withOpacity(.4),
        )
      ],
    );
  }
}
