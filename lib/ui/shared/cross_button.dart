import 'package:empire/constants/constants.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CrossButton extends HookWidget {
  final onPress;
  final text;
  final IconData? icon;

  const CrossButton(this.text, {required this.onPress, this.icon});

  @override
  Widget build(BuildContext context) {
    final working = useProvider(workProvider);
    return SizedBox(
      height: 44,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Const.color.primary,
          onPrimary: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        ),
        onPressed: onPress,
        child: working.when(
            data: (data) {
              return data.working
                  ? CircularProgressIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(text, style: Const.style.buttonText),
                        SizedBox(width: 8),
                        if (icon != null) Icon(icon),
                      ],
                    );
            },
            loading: () => CircularProgressIndicator(),
            error: (error, stack) => Text(text)),
      ),
    );
  }
}
