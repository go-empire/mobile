import 'package:empire/constants/constants.dart';
import 'package:empire/shared/contries.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SelectString extends StatefulWidget {
  @override
  _SelectStringState createState() => _SelectStringState();
}

class _SelectStringState extends State<SelectString> {
  var countrySelected;
  int countrySelectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: () {
          Navigator.pop(context, countrySelected);
        }),
        backgroundColor: Colors.transparent,
        title: Text("Sélection"),
      ),
      body: ListView.builder(
        itemCount: countries.length,
        itemBuilder: (context, int index) {
          return ListTile(
            onTap: () {
              setState(() {
                countrySelected = countries.elementAt(index);
                countrySelectedIndex = index;
              });
            },
            title: Text("${countries.elementAt(index)["name"]}", style: Const.style.bodyText),
            trailing: countrySelectedIndex == index ? Icon(FeatherIcons.checkCircle, color: Const.color.green) : SizedBox.shrink(),
          );
        },
      ),
    );
  }
}
