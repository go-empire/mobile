class AttachmentState {
  List<AttachmentFile> attachments = [];

  AttachmentState({
    required this.attachments,
  });

  AttachmentState copyWith({
    List<AttachmentFile>? attachments,
  }) {
    return new AttachmentState(
      attachments: attachments ?? this.attachments,
    );
  }
}

class AttachmentFile {
  String filePath;
  String filename;

  AttachmentFile({
    required this.filePath,
    required this.filename,
  });
}