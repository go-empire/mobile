import 'package:empire/constants/constants.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CrossBottomButton extends HookWidget {
  final onPress;
  final text;

  const CrossBottomButton(
    this.text, {
    required this.onPress,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final work = useProvider(workProvider);
    return Consumer(builder: (context, watch, _) {
      return InkWell(
        onTap: work.maybeWhen(data: (data) => data.working ? null : onPress, orElse: () => onPress),
        child: Container(
          color: Const.color.primary,
          height: 51,
          child: Center(
            child: work.maybeWhen(
              data: (data) {
                if (data.working) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(backgroundColor: Colors.white),
                      SizedBox(width: 10),
                      Text(data.message, style: Const.style.mediumText)
                    ],
                  );
                }
                return Text(text, style: Const.style.mediumWhite.copyWith(fontSize: 18));
              },
              orElse: () {
                return Text(text, style: Const.style.mediumWhite);
              },
            ),
          ),
        ),
      );
    });
  }
}
