import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/shared/utils.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/adherent/adherent_list.dart';
import 'package:empire/ui/adherent/search_adherents_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProjectItemSmall extends StatelessWidget {
  const ProjectItemSmall({
    Key? key,
    required this.project,
  }) : super(key: key);

  final Project project;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read(projectSelectedIdProvider).state = project.id;
        Navigator.pushNamed(context, Routes.projectDetail);
      },
      child: Stack(
        children: [
          Container(
            height: 100,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: CachedNetworkImageProvider(project.imageUrl ?? "https://source.unsplash.com/1600x900/?business,${project.title}"),
                  fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(7),
            ),
          ),
          Container(
            height: 100,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.25, 1],
                colors: [Colors.black54, Colors.transparent],
              ),
              borderRadius: BorderRadius.circular(7),
            ),
          ),
          Positioned(
            left: 16,
            top: 16,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(project.title!, style: TextStyle(color: Colors.white, fontSize: 16)),
                Text(
                  "${projectStatus[project.status]?.elementAt(0)}",
                  style: TextStyle(color: projectStatus[project.status]?.elementAt(1) as Color, fontSize: 14),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
          ),
          Positioned.fill(
            right: 20,
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(Icons.arrow_forward_ios_rounded, color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
