import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/project/project_item_small.dart';
import 'package:empire/ui/project/search_project_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AdminProjectsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.color.lightWhite,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Projets".toUpperCase(),
          style: Const.style.pageTitle.copyWith(color: Const.color.primary),
        ),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 700),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 15),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () async {
                    await Navigator.pushNamed(context, SearchProjectPage.path);
                    context.read(searchProjectTermProvider).state = "";
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 12),
                    height: 40,
                    decoration: BoxDecoration(
                      color: Const.color.primary,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Text(
                      "Rechercher",
                      style: Const.style.bodyText.copyWith(color: Const.color.lightWhite, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                Expanded(
                  child: Consumer(
                    builder: (BuildContext context, watch, Widget? child) {
                      List<Project> projects = watch(projectListNotifier.state).projects;

                      return ListView.separated(
                        itemBuilder: (context, index) {
                          Project project = projects.elementAt(index);
                          return ProjectItemSmall(project: project);
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 32);
                        },
                        itemCount: projects.length,
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
