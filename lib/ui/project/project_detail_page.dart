import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_project_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/shared/utils.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ProjectDetailPage extends HookWidget {
  const ProjectDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Project? project = useProvider(projectSelectedProvider).state;
    DateTime? dateParsed = project?.startDate;
    String date = "${dateParsed?.day}/${dateParsed?.month.toString().padLeft(2, '0')}/${dateParsed?.year}";
    Color? statusColor = projectStatus[project?.status ?? 0]?[1] as Color?;

    if (project == null) {
      return Material(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Une erreur est survenue"),
              IconButton(onPressed: () {
                context.refresh(projectSelectedProvider);
              }, icon: Icon(Icons.refresh_outlined)),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: 200,
            iconTheme: IconTheme.of(context).copyWith(color: Colors.white),
            flexibleSpace: FlexibleSpaceBar(
              title: Text("${project.title}"),
              background: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(project.imageUrl ?? "https://source.unsplash.com/1600x900/?business,${project.title}"),
                      fit: BoxFit.cover),
                ),
                child: Container(
                    decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff000000).withOpacity(0.6), Colors.transparent],
                    stops: [0.4, 1],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                )),
              ),
              // background: project.imageUrl != null ? CachedNetworkImage(imageUrl: project.imageUrl!, fit: BoxFit.cover) : Container(color: Colors.orange),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Status du projet", style: Const.style.mediumBodyText),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("${projectStatus[project.status]?[0]}", style: TextStyle(color: statusColor)),
                      Consumer(
                        builder: (BuildContext context, watch, Widget? child) {
                          User? user = watch(currentUserNotifier.state).user;
                          if (user?.isRepresentant == true) {
                            return SizedBox.shrink();
                          } else {
                            return ElevatedButton(
                              onPressed: () async {
                                int? result = await showModalActionSheet(
                                  context: context,
                                  title: "Mise à jour du status",
                                  actions: [
                                    SheetAction(label: "En cours de traitement", key: 1),
                                    SheetAction(label: "Accepté", key: 2),
                                    SheetAction(label: "Clôturé", key: 3),
                                    SheetAction(label: "Rejeté", key: 4),
                                  ],
                                );
                                if (result != null && result != project.status && project.id != null) {
                                  context.read(projectListNotifier).updateStatus(project.id!, result);
                                }
                              },
                              child: Text("Modifier le status"),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Text("Description", style: Const.style.mediumBodyText),
                  SizedBox(height: 5),
                  Text("${project.description}"),
                  SizedBox(height: 20),
                  Text("Date de début du projet", style: Const.style.mediumBodyText),
                  Text("$date"),
                  SizedBox(height: 20),
                  Text("Budget", style: Const.style.mediumBodyText),
                  Text("${project.budget} FCFA"),
                  SizedBox(height: 20),
                  Text("Durée du projet", style: Const.style.mediumBodyText),
                  Text("${project.duration} Mois"),
                  SizedBox(height: 20),
                  Text("Pièces jointes", style: Const.style.mediumBodyText),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
