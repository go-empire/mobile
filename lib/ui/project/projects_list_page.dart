import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/shared/utils.dart';
import 'package:empire/states/project_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ProjectListPage extends HookWidget {
  const ProjectListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final projectListState = useProvider(projectListNotifier.state);

    if (projectListState.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (projectListState.hasError) {
      return Center(
        child: Text("An error occured"),
      );
    }

    final projects = projectListState.projects;

    return Scaffold(
      backgroundColor: Const.color.background,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text("Projets", style: Theme.of(context).appBarTheme.titleTextStyle),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, Routes.newProject);
            },
            child: Text("Nouveau projet", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.separated(
            itemBuilder: (context, index) {
              final project = projects.elementAt(index);
              return GestureDetector(
                onTap: () {
                  context.read(projectSelectedIdProvider).state = project.id;
                  Navigator.pushNamed(context, Routes.projectDetail);
                },
                child: Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Stack(
                    children: [
                      Container(
                        height: 300,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            image: CachedNetworkImageProvider(project.imageUrl ?? "https://source.unsplash.com/1600x900/?business,${project.title}"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                        height: 300,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(
                            colors: [Color(0xff000000).withOpacity(0.6), Colors.transparent],
                            stops: [0.4, 1],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${projectStatus[project.status]?.elementAt(0)}",
                              style: TextStyle(color: projectStatus[project.status]?.elementAt(1) as Color, fontWeight: FontWeight.bold),
                            ),
                            Text("${project.title}", style: TextStyle(fontSize: 32, fontWeight: FontWeight.w900, color: Colors.white)),
                            Text("${project.description}", maxLines: 4, style: TextStyle(color: Colors.white), overflow: TextOverflow.ellipsis),
                          ],
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          width: double.infinity,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(top: Radius.zero, bottom: Radius.circular(10)), color: Const.color.secondary),
                          child: Center(
                            child: TextButton(
                              onPressed: () {
                                context.read(projectSelectedIdProvider).state = project.id;
                                Navigator.pushNamed(context, Routes.projectDetail);
                              },
                              child: Text("Détails", style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) => SizedBox(height: 10),
            itemCount: projects.length),
      ),
    );
  }
}
