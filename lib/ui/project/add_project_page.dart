import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/dto/create_project_dto.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:empire/ui/shared/attachment_cell.dart';
import 'package:empire/ui/shared/basic_form_field.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:empire/ui/shared/cross_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

class AddProjectPage extends HookWidget {
  AddProjectPage({Key? key}) : super(key: key);

  final _projectFormKey = GlobalKey<FormState>();

  _isValidate(FormGroup form) {
    if (form.hasErrors) {
      final fieldKey = form.errors.keys.elementAt(0);
      switch(fieldKey) {
        case 'title':
          Services.message.displayError("Le titre du projet est requis");
          break;
        case 'budget':
          Services.message.displayError("Budget invalide");
          break;
        case 'duration':
          Services.message.displayError("Durée non valide, veuillez saisir un nombre valide");
          break;
        case 'startDate':
          Services.message.displayError("La date n'est pas valide");
      }
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {

    final projectAttachments = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconTheme.of(context).copyWith(color: Const.color.primary),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Nouveau projet".toUpperCase(), style: Const.style.pageTitle),
        centerTitle: true,
      ),
      bottomNavigationBar: CrossBottomButton("Créer le projet", onPress: () async {

        FocusScope.of(context).unfocus();
        Services.work.start();
        final form = context.read(projectFormProvider);

        if (!_isValidate(form)) {
          Services.work.stop();
          return;
        }

        final attachments = projectAttachments.value.attachments.map((AttachmentFile attachment) {
          return CreateFileDto.simple("attachment", attachment.filename, attachment.filePath);
        }).toList(growable: false);

        final success = await context.read(projectListNotifier).create(attachments);
        if (success) {
          context.read(projectFormProvider).reset();
          Navigator.pop(context);
        } else {

        }
        Services.work.stop();
      }),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: ReactiveForm(
            formGroup: useProvider(projectFormProvider),
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BasicFormField(FeatherIcons.edit2, "Titre du projet", 'title', formKey: _projectFormKey,),
                  SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 18, top: 20),
                    child: Row(
                      children: [
                        Icon(FeatherIcons.alignLeft),
                        SizedBox(
                          width: 16,
                        ),
                        Text("Description du projet", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.w100, fontSize: 12)),
                      ],
                    ),
                  ),
                  Container(
                    child: ReactiveTextField(
                      formControlName: 'description',
                      validationMessages: (_) => { "required": "Veuillez renseigner une petite description" },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                        borderSide: BorderSide(color: Const.color.primary, width: 2),
                        borderRadius: BorderRadius.circular(30),
                      )),
                      style: Const.style.mediumBodyText,
                      minLines: 8,
                      maxLines: 8,
                    ),
                  ),
                  SizedBox(height: 24),
                  BasicFormField(FeatherIcons.dollarSign, "Budget", 'budget', inputType: TextInputType.number),
                  SizedBox(height: 24),
                  BasicFormField(FeatherIcons.calendar, "Date début", 'startDate', inputType: TextInputType.datetime),
                  SizedBox(height: 24),
                  BasicFormField(FeatherIcons.trendingUp, "Durée (mois)", 'duration', inputType: TextInputType.number),
                  SizedBox(height: 24),
                  ValueListenableBuilder(
                    valueListenable: projectAttachments,
                    builder: (BuildContext context, AttachmentState value, Widget? child) {
                      return Container(
                        color: Colors.white,
                        child: Column(
                          children: List.generate(projectAttachments.value.attachments.length, (index) {
                            final lastCell = index == projectAttachments.value.attachments.length - 1;
                            return AttachmentCell(state: projectAttachments, index: index, hideLine: lastCell);
                          }),
                        ),
                      );
                    },
                  ),
                  Center(
                    child: SizedBox(
                      width: double.infinity,
                      child: CrossButton("Ajouter une pièce jointe", icon: FeatherIcons.link, onPress: () async {
                        final result = await FilePicker.platform.pickFiles(
                          type: FileType.custom,
                          allowedExtensions: ["pdf", "doc", "docx", "jpg", "png"],
                        );
                        if (result != null) {
                          final file = AttachmentFile(filePath: result.files.elementAt(0).path!, filename: result.files.elementAt(0).name);
                          final attachments = projectAttachments.value.attachments;
                          attachments.add(file);
                          projectAttachments.value = projectAttachments.value.copyWith(attachments: attachments);
                        }
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
