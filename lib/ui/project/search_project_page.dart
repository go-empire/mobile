import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/project/project_item_small.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SearchProjectPage extends StatelessWidget {
  static const path = '/search/project';
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.color.lightWhite,
        elevation: 0,
        centerTitle: true,
        title: Text("Rechercher un projet", style: Const.style.pageTitle.copyWith(color: Const.color.primary)),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 15),
          child: Column(
            children: [
              TextField(
                autofocus: true,
                onChanged: (String text) {
                  context.read(searchProjectTermProvider).state = text;
                },
                decoration: InputDecoration(filled: true, border: OutlineInputBorder()),
              ),
              SizedBox(height: 25),
              Consumer(builder: (context, watch, _) {
                final projects = watch(projectsFilteredProvider).state;
                return ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        context.read(projectSelectedProvider).state = projects.elementAt(index);
                        Navigator.pushNamed(context, Routes.projectDetail);
                      },
                      child: ProjectItemSmall(
                        project: projects.elementAt(index),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 20);
                  },
                  itemCount: projects.length,
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
