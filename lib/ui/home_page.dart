import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/local_notification_state.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/adherent/adherents_page.dart';
import 'package:empire/ui/dashboard/admin_dashboard.dart';
import 'package:empire/ui/dashboard/dashboard_page.dart';
import 'package:empire/ui/project/admin_projects_page.dart';
import 'package:empire/ui/users/users_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final bottomBarItemSelected = StateProvider((_) => 0);

class HomePage extends HookWidget {
  final List<IconData> icons = [FeatherIcons.home, FeatherIcons.userCheck, FeatherIcons.layout, FeatherIcons.users];
  final List<String> labels = ["Accueil", "Adhérents", "Projets", "Utilisateurs"];
  final List<String> pages = ["Adhérents", "Projets", "Utilisateurs"];

  @override
  Widget build(BuildContext context) {

    useEffect(() {
      final io = Services.io;
      context.read(localNotificationsProvider).init();
      Services.push.subscribeToTopic('admin-notifications');
    }, []);

    int bottomBarItemSelectedIndex = useProvider(bottomBarItemSelected).state;
    return useProvider(userFuture).when(
        data: (User user) {
          return Scaffold(
            bottomNavigationBar: !user.isRepresentant!
                ? BottomNavigationBar(
                    elevation: 0,
                    currentIndex: bottomBarItemSelectedIndex,
                    onTap: (index) {
                      context.read(bottomBarItemSelected).state = index;
                    },
                    items: List.generate(4, (index) {
                      return BottomNavigationBarItem(
                        icon: Icon(icons[index]),
                        label: labels[index],
                      );
                    }),
                  )
                : null,
            body: Consumer(
              builder: (context, watch, _) {
                switch (bottomBarItemSelectedIndex) {
                  case 0:
                    print(user.isRepresentant);
                    return user.isRepresentant! ? DashboardPage() : AdminDashboard();
                  case 1:
                    return AdherentsPage();
                  case 2:
                    return AdminProjectsPage();
                  case 3:
                    return UsersPage();
                  default:
                    return user.isRepresentant! ? DashboardPage() : AdminDashboard();
                }
              },
            ),
          );
        },
        loading: () => Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
        error: (error, stack) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Une erreur est survenue"),
                ElevatedButton(
                  onPressed: () {
                    context.refresh(userFuture);
                  },
                  child: Text("Ressayer"),
                ),
              ],
            ),
          );
        });
  }
}
