import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/users/user_details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class UsersPage extends StatelessWidget {
  static const path = '/users';

  const UsersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.color.background,
      appBar: AppBar(
        title: Text("Utilisateurs", style: Theme.of(context).appBarTheme.titleTextStyle),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: Consumer(
        builder: (context, watch, _) {
          final usersListState = watch(usersNotifier.state);
          if (usersListState.hasError) {
            Services.message.displayMessage("Une erreur est survenue");
            return Center(
              child: IconButton(
                onPressed: () {
                  context.refresh(usersNotifier);
                },
                icon: Icon(Icons.refresh_outlined),
              ),
            );
          }

          if (usersListState.isLoading) {
            return Center(
              child: CircularProgressIndicator(color: Colors.white),
            );
          }

          final users = usersListState.users;
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ListView.separated(
              itemBuilder: (context, index) {
                final user = users.elementAt(index);

                return GestureDetector(
                  onTap: () {
                    context.read(userSelectedProvider).state = user;
                    Navigator.pushNamed(context, UserDetailsPage.path);
                  },
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(200),
                          color: Const.color.primary,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Container(
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: user.profilePictureUrl != null
                                ? CachedNetworkImage(imageUrl: user.profilePictureUrl ?? "", fit: BoxFit.cover)
                                : Container(color: Const.color.secondary),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${user.fullname}",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                          Text("${user.roles}"),
                        ],
                      )
                    ],
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return SizedBox(height: 16);
              },
              itemCount: users.length,
            ),
          );
        },
      ),
    );
  }
}
