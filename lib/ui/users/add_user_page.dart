import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/representant_state.dart';
import 'package:empire/ui/adherent/profile_picture.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:empire/ui/shared/attachment_cell.dart';
import 'package:empire/ui/shared/basic_form_field.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:empire/ui/shared/cross_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AddUserPage extends StatefulWidget {
  static const path = "/users/add";

  const AddUserPage({Key? key}) : super(key: key);

  @override
  _AddUserPageState createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  final profileState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));

  _displayError(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(text)),
    );
  }

  _validatedRepresentant(FormGroup form) {
    if (form.hasErrors) {
      final field = form.errors.keys.elementAt(0);
      var errorMessage;
      switch (field) {
        case 'fullname':
          errorMessage = "Vous devez renseigner le nom complet du représentant";
          break;
        case 'email':
          errorMessage = "L'email doit être valide pour l'envoi du mot de passe";
          break;
        default:
          errorMessage = "Une erreur est survenue";
      }
      _displayError(errorMessage);
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text("Ajouter un utilisateur", style: Theme.of(context).appBarTheme.titleTextStyle),
      ),
      bottomNavigationBar: CrossBottomButton("Ajouter l'utilisateur", onPress: () async {
        final form = context.read(representantFormProvider);
        if (!_validatedRepresentant(form)) return;

        Services.work.start();
        FocusScope.of(context).unfocus();

        final profilePicture = profileState.value.attachments.isNotEmpty ? profileState.value.attachments.first : null;

        final createProfileFile = profilePicture != null ? CreateFileDto.simple("profile", profilePicture.filename, profilePicture.filePath) : null;

        final adherent = context.read(adherentSelectedProvider).state;
        try {
          final success = await context.read(adherentProvider).createRepresentant(
                adherent.id!,
                null,
                createProfileFile,
              );

          if (success) {
            Navigator.pop(context);
            Services.work.stop();
          } else {
            Services.work.stop();
          }
        } catch (e) {
          Services.work.stop();
        }
      }),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 48.h, left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              ValueListenableBuilder(
                valueListenable: profileState,
                builder: (BuildContext context, AttachmentState value, Widget? child) {
                  return ProfilePicture(state: profileState);
                },
              ),
              SizedBox(height: 36),
              Consumer(
                builder: (context, watch, _) {
                  final form = watch(representantFormProvider);
                  return ReactiveForm(
                    formGroup: form,
                    child: Column(
                      children: [
                        BasicFormField(FeatherIcons.user, "Nom et prénoms *", 'fullname'),
                        SizedBox(height: 24),
                        BasicFormField(FeatherIcons.mail, "Email *", 'email'),
                        SizedBox(height: 24),
                        BasicFormField(FeatherIcons.phone, "Téléphone", 'phone'),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
