import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/users/update_user_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';

class UserDetailsPage extends StatelessWidget {
  static const path = "/users/details";

  const UserDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.color.background,
      appBar: AppBar(
        title: Text("Profile", style: Theme.of(context).appBarTheme.titleTextStyle),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Consumer(
          builder: (context, watch, _) {
            final userSelected = watch(userSelectedProvider).state;

            if (userSelected == null) {
              return Center(
                child: Text("Une erreur est survenue"),
              );
            }

            final request = watch(userDetailsFutureProvider(userSelected.id!));
            return request.when(
              data: (User? user) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                            color: Const.color.lightWhite,
                            borderRadius: BorderRadius.circular(200),
                            boxShadow: [BoxShadow(offset: Offset(0, 4), color: Colors.black.withOpacity(.16), blurRadius: 6)]),
                        child: CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(user?.profilePictureUrl ?? "https://picsum.photos/200"),
                          radius: 50,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text("${user?.fullname}"),
                      ),
                      if (user?.isRepresentant ?? true)
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text("${user?.email}"),
                        ),
                      if (user?.isRepresentant ?? true)
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text("250 pts"),
                        ),
                      if (!(user?.isRepresentant ?? true))
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text("${user?.job}"),
                        ),
                      if (!(user?.isRepresentant ?? true))
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text("Admin"),
                        ),
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (user?.phone != null)
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                    onPressed: () async {
                                      String phoneUrl = "tel://${user?.phone ?? ""}";
                                      if (await canLaunch(phoneUrl)) {
                                        launch(phoneUrl);
                                      } else {
                                        Services.message.displayError("Une erreur est survenue");
                                      }
                                    },
                                    child: Icon(Icons.phone_forwarded_outlined),
                                    style: ElevatedButton.styleFrom(shape: CircleBorder(), padding: EdgeInsets.zero),
                                  ),
                                  Text("Tel")
                                ],
                              ),
                            ),
                          SizedBox(
                            width: 36
                          ),
                          if (user?.email != null)
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                    onPressed: () async {
                                      String mailto = "mailto://${user?.email ?? ""}";
                                      if (await canLaunch(mailto)) {
                                        launch(mailto);
                                      } else {
                                        Services.message.displayError("Une erreur est survenue");
                                      }
                                    },
                                    child: Icon(Icons.email_outlined),
                                    style: ElevatedButton.styleFrom(
                                      shape: CircleBorder(),
                                      // padding: EdgeInsets.zero,
                                    ),
                                  ),
                                  Text("Email", style: TextStyle(fontSize: 12))
                                ],
                              ),
                            ),
                        ],
                      ),
                      SizedBox(height: 48),
                      Container(
                        width: 216,
                        height: 40,
                        child: ElevatedButton(
                          child: Text("Modifier"),
                          onPressed: () {
                            Navigator.pushNamed(context, UpdateUserPage.path);
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
              loading: () => CircularProgressIndicator(),
              error: (stack, err) => Text("Une erreur est survenue"),
            );
          },
        ),
      ),
    );
  }
}
