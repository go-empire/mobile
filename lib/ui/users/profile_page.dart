import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfilePage extends StatelessWidget {

  static const path = "/users/profile";

  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.color.background,
      appBar: AppBar(
        title: Text("Profile", style: Theme.of(context).appBarTheme.titleTextStyle),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Consumer(
          builder: (context, watch, _) {
            final userState = watch(currentUserNotifier.state);
            if (userState.hasError) {
              return Center(
                child: Text("Une erreur est survenue"),
              );
            }

            if (userState.isLoading) {
              return Center(
                child: CircularProgressIndicator(color: Colors.white),
              );
            }

            final user = userState.user;
            return ProviderListener(
              provider: authStateProvider.state,
              onChange: (context, AuthState state) async {
                if (state.signedOut) {
                  context.read(authStateProvider).initialize();
                  await Navigator.of(context).pushNamed(Routes.signIn);
                }
              },
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(1),
                      decoration: BoxDecoration(
                          color: Const.color.lightWhite,
                          borderRadius: BorderRadius.circular(200),
                          boxShadow: [BoxShadow(offset: Offset(0, 4), color: Colors.black.withOpacity(.16), blurRadius: 6)]),
                      child: CircleAvatar(
                        backgroundImage: CachedNetworkImageProvider(user?.profilePictureUrl ?? "https://picsum.photos/200"),
                        radius: 80,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text("${user?.firstname} ${user?.lastname}"),
                    ),
                    if (user?.isRepresentant ?? true)
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text("${user?.email}"),
                      ),
                    if (user?.isRepresentant ?? true)
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text("250 pts"),
                      ),
                    if (!(user?.isRepresentant ?? true))
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text("${user?.job}"),
                      ),
                    if (!(user?.isRepresentant ?? true))
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text("Admin"),
                      ),
                    SizedBox(height: 100),
                    Container(
                      width: 216,
                      height: 40,
                      child: ElevatedButton(
                        child: Text("Déconnexion"),
                        onPressed: () {
                          context.read(authStateProvider).signOut();
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
