import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/dashboard/recent_articles.dart';
import 'package:empire/ui/dashboard/recent_projects.dart';
import 'package:empire/ui/points/user_points_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'header.dart';

class DashboardPage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.color.background,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 18.h, left: 24.w, right: 24.w, bottom: 24),
            child: Container(
              child: Column(
                children: [
                  Header(),
                  SizedBox(height: 16),
                  Column(
                    children: [
                      Consumer(
                        builder: (BuildContext context, watch, Widget? child) {
                          Adherent? adherent = watch(currentUserAdherentProvider).state;
                          return RichText(
                            text: TextSpan(children: [
                              TextSpan(text: '${adherent?.points ?? 000 }', style: TextStyle(fontSize: 72, color: Const.color.primary, fontWeight: FontWeight.bold)),
                              TextSpan(text: (adherent?.points ?? 0) > 1 ? 'pts' : 'pt', style: TextStyle(fontSize: 12, color: Const.color.primary, fontWeight: FontWeight.bold)),
                            ]),
                          );
                        },
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        width: 100,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              )),
                          onPressed: () {
                            Navigator.pushNamed(context, UserPointsPage.path);
                          },
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Détails'),
                                Flexible(child: Icon(Icons.arrow_forward_ios_rounded, size: 18,)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 32),
                  RecentArticles(),
                  SizedBox(height: 20),
                  RecentProjects(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}