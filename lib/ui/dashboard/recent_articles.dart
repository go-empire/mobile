import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/article_state.dart';
import 'package:empire/states/project_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class RecentArticles extends HookWidget {
  const RecentArticles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final articlesState = useProvider(articleListProvider.state);

    if (articlesState.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (articlesState.hasError) {
      return Center(
        child: Text("Une erreur est survenue lors de la récupération des articles"),
      );
    }

    final articles = articlesState.articles;

    if (articles.isEmpty) {
      return Center(
        child: Text("Aucun article disponible"),
      );
    }

    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Articles', style: Const.style.mediumBodyText),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, Routes.articleList);
                },
                child: Text('Afficher tout'),
              ),
            ],
          ),
          SizedBox(height: 10),
          SingleChildScrollView(
            primary: false,
            scrollDirection: Axis.horizontal,
            child: Wrap(
                spacing: 20,
                children: List.generate(articles.length > 5 ? 5 : articles.length, (index) {
                  final article = articles.elementAt(index);
                  return GestureDetector(
                    onTap: () {
                      context.read(articleSelectedProvider).state = index;
                      Navigator.pushNamed(context, Routes.articleDetail);
                    },
                    child: Container(
                      height: 200.w,
                      width: 200.w,
                      decoration: BoxDecoration(
                        image:
                            article.imageUrl != null ? DecorationImage(image: CachedNetworkImageProvider(article.imageUrl!), fit: BoxFit.cover) : null,
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Colors.black54, Colors.transparent],
                            stops: [0.3, 1],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("${article.title}", style: TextStyle(color: Colors.white, fontSize: 14)),
                        ),
                      ),
                    ),
                  );
                }).toList()),
          ),
        ],
      ),
    );
  }
}
