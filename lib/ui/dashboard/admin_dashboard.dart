import 'package:empire/constants/constants.dart';
import 'package:empire/ui/dashboard/recent_projects.dart';
import 'package:flutter/material.dart';
import 'package:empire/ui/dashboard/kpi.dart';
import 'package:empire/ui/dashboard/header.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AdminDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 18.h, left: 24.w, right: 24.w, bottom: 24),
            child: Column(
              children: [
                Header(),
                SizedBox(height: 45),
                KPI(),
                SizedBox(height: 32),
                RecentProjects(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
