import 'package:empire/constants/constants.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/project_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class KPI extends StatelessWidget {
  static const List<String> kpiLabels = ["Tous les\nadhérents", "Nouveaux\nadhérents", "Tous les \nProjets", "Nouveaux\nprojets"];
  static const List<Color> labelColors = [Color(0xff3383E2), Color(0xff30E55A), Color(0xffE23D3D), Color(0xffD5E23D)];

  const KPI({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        final adherents = watch(adherentProvider.state).adherents;
        final recentAdherents = adherents?.where((adherent) => adherent.createdAt != null ? DateTime.now().difference(adherent.createdAt!)  < Duration(days: 2): false).toList();
        final projects = watch(projectListNotifier.state).projects;
        final recentProjects = projects.where((project) => project.createdAt == null ? false : DateTime.now().difference(project.createdAt!)  < Duration(days: 2)).toList();
        List<int> kpiValues = [adherents?.length ?? 0, recentAdherents?.length ?? 0, projects.length, recentProjects.length];

        return GridView.count(
          primary: false,
          shrinkWrap: true,
          crossAxisCount: 2,
          crossAxisSpacing: 21,
          mainAxisSpacing: 21,
          children: List.generate(4, (index) {
            return KpiItem(index, kpiLabels.elementAt(index), kpiValues.elementAt(index), labelColors.elementAt(index));
          }),
        );
      },
    );
  }
}

class KpiItem extends StatelessWidget {
  final int index;
  final String label;
  final int value;
  final Color color;

  const KpiItem(
    this.index,
    this.label,
    this.value,
    this.color, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Const.color.secondary,
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 15.w, top: 15.w, bottom: 15.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              flex: 3,
              fit: FlexFit.tight,
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text("$value", style: Const.style.kpiText.copyWith(color: Const.color.lightWhite, height: 1)),
              ),
            ),
            Flexible(
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(label, style: Const.style.kpiLabel.copyWith(color: Const.color.lightWhite)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
