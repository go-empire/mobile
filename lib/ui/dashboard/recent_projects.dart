import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/shared/utils.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class RecentProjects extends StatelessWidget {
  const RecentProjects({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Consumer(
              builder: (BuildContext context, watch, Widget? child) {
                User? user = watch(currentUserNotifier.state).user;
                return user?.isRepresentant == true ? Text('Mes projets soumis', style: Const.style.mediumBodyText) : Text('Projets récents', style: Const.style.mediumBodyText);
              },
            ),
            TextButton(
              onPressed: () {
                Navigator.pushNamed(context, Routes.projectList);
              },
              child: Text('Afficher tout'),
            ),
          ],
        ),
        ProjectList(),
      ],
    );
  }
}

class ProjectList extends HookWidget {
  const ProjectList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final projects = useProvider(projectListNotifier.state).projects;
    return Wrap(
      runSpacing: 20,
      children: List.generate(projects.length > 3 ? 3 : projects.length, (index) {
        final project = projects.elementAt(index);
        return GestureDetector(
          onTap: () {
            context.read(projectSelectedIdProvider).state = project.id;
            Navigator.pushNamed(context, Routes.projectDetail);
          },
          child: Stack(
            children: [
              Container(
                height: 100,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(project.imageUrl ?? "https://source.unsplash.com/1600x900/?business,${project.title}"),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(7),
                ),
              ),
              Container(
                height: 100,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.25, 1],
                    colors: [Colors.black54, Colors.transparent],
                  ),
                  borderRadius: BorderRadius.circular(7),
                ),
              ),
              Positioned(
                left: 16,
                top: 16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(project.title!, style: TextStyle(color: Colors.white, fontSize: 16)),
                    Text(
                      "${projectStatus[project.status]?.elementAt(0)}",
                      style: TextStyle(color: projectStatus[project.status]?.elementAt(1) as Color, fontSize: 14),
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                right: 20,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.arrow_forward_ios_rounded, color: Colors.white),
                ),
              )
            ],
          ),
        );
      }).toList(),
    );
  }
}
