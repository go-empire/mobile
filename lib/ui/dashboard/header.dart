import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/notification/notifications_page.dart';
import 'package:empire/ui/users/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Header extends HookWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user$ = useProvider(currentUserNotifier.state);

    if (user$.isLoading) {
      return CircularProgressIndicator();
    }

    if (user$.hasError) {
      return Container();
    }

    final user = user$.user;
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, ProfilePage.path);
          },
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.all(3),
               decoration: BoxDecoration(
                 color: Const.color.primary,
                 borderRadius: BorderRadius.circular(200),
               ),
                child: CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(user?.profilePictureUrl ?? "https://picsum.photos/200"),
                  radius: 15,
                ),
              ),
              SizedBox(width: 12),
              Text("Bienvenu\n${user?.firstname ?? user?.fullname}", textAlign: TextAlign.start, style: TextStyle(fontSize: 12))
            ],
          ),
        ),
        Spacer(),
        Row(
          children: [
            IconButton(
              icon: Icon(Icons.qr_code_scanner),
              iconSize: 24.w,
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              onPressed: () async {
                await Navigator.pushNamed(context, Routes.qrcode);
              },
            ),
            // IconButton(
            //   icon: Icon(FeatherIcons.maximize),
            //   iconSize: 24.w,
            //   padding: EdgeInsets.symmetric(horizontal: 12.w),
            //   onPressed: () {},
            // ),
            IconButton(
              icon: Icon(FeatherIcons.bell),
              iconSize: 24.w,
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              onPressed: () async {
                await Navigator.pushNamed(context, NotificationPage.path);
              },
            ),
            IconButton(
              icon: Icon(FeatherIcons.messageSquare),
              iconSize: 24.w,
              padding: EdgeInsets.only(left: 12.w),
              onPressed: () async {
                Conversation? conversation = context.read(conversationSelectedProvider).state;

                // for representant we are getting his own conversation, for admin therefore we get all others conversations
                if (conversation == null) {
                  User user = context.read(currentUserNotifier.state).user as User;
                  conversation = await Repositories.conversation.userConversation(user.id!);
                  context.read(conversationSelectedProvider).state = conversation;
                }

                await Navigator.pushNamed(context, Routes.chat);
                context.read(conversationSelectedProvider).state = null;
              },
            ),
          ],
        ),
      ],
    );
  }
}
