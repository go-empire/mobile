import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

class StarSelection extends StatefulWidget {
  final IconData icon;
  final String prefix;
  final bool hideLine;

  const StarSelection(
      this.icon,
      this.prefix, {
        Key? key,
        this.hideLine = false,
      }) : super(key: key);

  @override
  _StarSelectionState createState() => _StarSelectionState();
}

class _StarSelectionState extends State<StarSelection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: Const.color.primary),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Row(
              children: [
                Icon(widget.icon, color: Const.color.primary),
                SizedBox(width: 25),
                Text(widget.prefix, style: Const.style.mediumBodyText.copyWith(color: Const.color.primary)),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      await Navigator.pushNamed(context, Routes.selectStar);
                    },
                    child: Consumer(
                      builder: (context, watch, _) {
                        final field = watch(adherentFormProvider).control('adherent.level');
                        return Container(
                          height: 48,
                          child: field.value != ""
                              ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(field.value, style: Const.style.mediumBodyText.copyWith(fontSize: 15)),
                              ),
                              Icon(Icons.star, color: Const.color.primary, size: 14),
                              Icon(Icons.expand_more, color: Const.color.primary),
                            ],
                          )
                              : Align(alignment: Alignment.centerRight, child: Icon(Icons.expand_more, color: Const.color.primary)),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 75),
            child: Container(
              height: widget.hideLine ? 0 : 1,
              color: Const.color.textBody,
            ),
          )
        ],
      ),
    );
  }
}