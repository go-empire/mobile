import 'package:collection/src/iterable_extensions.dart';
import 'package:empire/models/category.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/category_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CategoryField extends StatefulWidget {
  final IconData icon;
  final String prefix;
  final bool hideLine;

  const CategoryField(
    this.icon,
    this.prefix, {
    Key? key,
    this.hideLine = false,
  }) : super(key: key);

  @override
  _CategoryFieldState createState() => _CategoryFieldState();
}

class _CategoryFieldState extends State<CategoryField> {
  String? categoryId;

  @override
  void initState() {
    super.initState();
    final form = context.read(adherentFormProvider);
    categoryId = form.control('adherent.categoryId').value;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        Category? category = watch(categoriesStateNotifier.state).categories.singleWhereOrNull((cat) => cat.id == categoryId);

        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: Const.color.primary),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  children: [
                    Icon(widget.icon, color: Const.color.primary),
                    SizedBox(width: 25),
                    Text(widget.prefix, style: Const.style.mediumBodyText),
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          context.read(categoryIdSelectedProvider).state = categoryId;
                          categoryId = await Navigator.pushNamed(context, Routes.selectCategory) as String?;
                          if (categoryId != null) {
                            final form = context.read(adherentFormProvider);
                            form.control('adherent.categoryId').value = categoryId;
                          }
                        },
                        child: Container(
                          height: 48,
                          child: categoryId != null
                              ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text("${category?.name}", style: Const.style.mediumBodyText),
                              ),
                              Icon(FeatherIcons.chevronRight),
                            ],
                          )
                              : Align(alignment: Alignment.centerRight, child: Icon(Icons.expand_more)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 75),
                child: Container(
                  height: 1,
                  color: Const.color.textBody.withOpacity(.40),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
