import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/adherent/adherent_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SearchAdherentPage extends StatelessWidget {

  static const path = '/search/adherents';
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.color.lightWhite,
        elevation: 0,
        centerTitle: true,
        title: Text("Rechercher un adhérent", style: Const.style.pageTitle.copyWith(color: Const.color.primary)),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 15),
          child: Column(
            children: [
              TextField(
                autofocus: true,
                onChanged: (String text) {
                  context.read(searchTermProvider).state = text;
                },
                decoration: InputDecoration(
                  filled: true,
                  border: OutlineInputBorder()
                ),
              ),
              SizedBox(height: 25),
              Consumer(
                builder: (context, watch, _) {
                  final adherents = watch(adherentsFilteredProvider).state;
                  return ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            context.read(adherentDetailsProvider).state = index;
                            Navigator.pushNamed(context, Routes.adherentDetails);
                          },
                          child: AdherentItem(adherents.elementAt(index)));
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 20);
                    },
                    itemCount: adherents.length,
                  );
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
