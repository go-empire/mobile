import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/services/services.dart';
import 'package:empire/shared/input-formatter.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/adherent/category_field.dart';
import 'package:empire/ui/adherent/start_selection.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:empire/ui/shared/attachment_cell.dart';
import 'package:empire/ui/adherent/profile_picture.dart';

import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/ui/adherent/select_country_field.dart';
import 'package:empire/ui/shared/basic_form_field.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:empire/ui/shared/cross_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reactive_forms/reactive_forms.dart';

class AddAdherentPage extends StatefulWidget {
  @override
  _AddAdherentPageState createState() => _AddAdherentPageState();
}

class _AddAdherentPageState extends State<AddAdherentPage> {
  final errors = [];

  final profileState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));
  final adherentAttachmentState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));
  final userAttachmentState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));

  _displayError(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(text)),
    );
  }

  _validatedAdherent(FormGroup form) {
    if (form.control('adherent').hasErrors) {
      final field = form.control('adherent').errors.keys.elementAt(0);
      var errorMessage;
      switch(field) {
        case 'name':
          errorMessage = "Le nom de l'adhérent est requis";
          break;
        case 'revenue':
          errorMessage = "Le revenu est requis et constitué de chiffres";
          break;
        case 'country':
          errorMessage = "Vous devez choisir le pays";
          break;
        case 'category':
          errorMessage = "Vous devez choisir une catégorie";
          break;
        default:
          errorMessage = "Une erreur est survenue";
      }
      _displayError(errorMessage);
      return false;
    }

    return true;
  }

  _validatedRepresentant(FormGroup form) {
    if (form.control('representant').hasErrors) {
      final field = form.control('representant').errors.keys.elementAt(0);
      var errorMessage;
      switch(field) {
        case 'fullname':
          errorMessage = "Vous devez renseigner le nom complet du représentant";
          break;
        case 'email':
          errorMessage = "L'email doit être valide pour l'envoi du mot de passe";
          break;
        default:
          errorMessage = "Une erreur est survenue";
      }
      _displayError(errorMessage);
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconTheme.of(context).copyWith(color: Const.color.primary),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text("Ajouter un adhérent".toUpperCase(), style: Const.style.pageTitle),
          centerTitle: true,
        ),
        bottomNavigationBar: CrossBottomButton("Ajouter l'adhérent", onPress: () async {
          final form = context.read(adherentFormProvider);
          print(form.errors);
          if (!_validatedAdherent(form)) return;
          if (!_validatedRepresentant(form)) return;

          Services.work.start();
          FocusScope.of(context).unfocus();

          final profilePicture = profileState.value.attachments.isNotEmpty ? profileState.value.attachments.first : null;
          final adherentAttachments = adherentAttachmentState.value.attachments;
          final userAttachments = userAttachmentState.value.attachments;

          final createProfileFile = profilePicture != null ? CreateFileDto.simple("profile", profilePicture.filename, profilePicture.filePath) : null;
          final createAdherentAttachments = adherentAttachments.isNotEmpty
              ? adherentAttachments.map((AttachmentFile attachment) {
                  return CreateFileDto.simple("attachment", attachment.filename, attachment.filePath);
                }).toList(growable: false)
              : null;
          final createRepresentantAttachments = userAttachments.isNotEmpty
              ? userAttachments.map((AttachmentFile attachment) {
                  return CreateFileDto.simple("attachment", attachment.filename, attachment.filePath);
                }).toList(growable: false)
              : null;

          final success = await context.read(adherentProvider).create(
            profilePicture: createProfileFile,
            adherentAttachments: createAdherentAttachments,
            representantAttachments: createRepresentantAttachments,
          );

          if (success) {
            Navigator.pop(context);
            Services.work.stop();
          } else {
            Services.work.stop();
          }
        }),
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 48.h, left: 16, right: 16),
          child: Consumer(
            builder: (context, watch, _) {
              final form = watch(adherentFormProvider);
              print(form.value);
              return ReactiveForm(
                formGroup: form,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 24),
                    ProfilePicture(state: profileState),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Entreprise", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    // adherent informations
                    Container(
                      child: Column(
                        children: [
                          BasicFormField(FeatherIcons.user, "Nom *", 'adherent.name'),
                          SizedBox(height: 12),
                          BasicFormField(
                            FeatherIcons.dollarSign,
                            "Chiffre d'affaiare",
                            "adherent.revenue",
                            inputType: TextInputType.number,
                            // inputFormatters: [SpacedNumberFormatter()],
                          ),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.phone, "Téléphone", 'adherent.phone', inputType: TextInputType.phone),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.mail, "Email", 'adherent.email', inputType: TextInputType.emailAddress),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.mapPin, "Adresse", 'adherent.address'),
                          SizedBox(height: 12),
                          SelectCountryField(FeatherIcons.globe, "Pays *", hideLine: true),
                        ],
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    SizedBox(height: 32),
                    Container(
                      child: Column(
                        children: [
                          CategoryField(FeatherIcons.alignJustify, "Catégorie *"),
                          SizedBox(height: 12),
                          StarSelection(FeatherIcons.star, "Niveau", hideLine: true)
                        ],
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Activités", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    Container(
                      child: ReactiveTextField(
                        formControlName: 'adherent.activity',
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Const.color.primary, width: 2),
                              borderRadius: BorderRadius.circular(30),
                            )),
                        style: Const.style.mediumBodyText,
                        minLines: 8,
                        maxLines: 8,
                      ),
                    ),
                    SizedBox(height: 32),
                    if (adherentAttachmentState.value.attachments.isNotEmpty)
                      Center(
                        child: Container(
                          width: .5.sw,
                          height: 2,
                          color: Const.color.primary,
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Documents", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    Container(
                      color: Colors.white,
                      child: Column(
                        children: List.generate(adherentAttachmentState.value.attachments.length, (index) {
                          final lastCell = index == adherentAttachmentState.value.attachments.length - 1;
                          return AttachmentCell(state: adherentAttachmentState, index: index, hideLine: lastCell);
                        }),
                      ),
                    ),
                    SizedBox(height: 16),
                    Center(
                      child: SizedBox(
                        width: double.infinity,
                        child: CrossButton("Ajouter une pièce jointe", icon: FeatherIcons.link, onPress: () async {
                          final result = await FilePicker.platform.pickFiles(
                            type: FileType.custom,
                            allowedExtensions: ["pdf", "doc", "docx", "jpg", "png"],
                          );
                          if (result != null) {
                            final file = AttachmentFile(filePath: result.files.elementAt(0).path!, filename: result.files.elementAt(0).name);
                            final attachments = adherentAttachmentState.value.attachments;
                            attachments.add(file);
                            adherentAttachmentState.value = adherentAttachmentState.value.copyWith(attachments: attachments);
                          }
                        }),
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Représentant", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    // Representant informations
                    Container(
                      child: Column(
                        children: [
                          BasicFormField(FeatherIcons.user, "Nom et prénoms", 'representant.fullname'),
                          SizedBox(height: 12),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          BasicFormField(FeatherIcons.phone, "Téléphone", 'representant.phone', inputType: TextInputType.phone),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.mail, "Email", 'representant.email', inputType: TextInputType.emailAddress),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.briefcase, "Poste", 'representant.job', hideLine: true),
                          SizedBox(height: 12),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    if (userAttachmentState.value.attachments.isNotEmpty)
                      Padding(
                        padding: const EdgeInsets.only(bottom: 18, top: 20),
                        child: Text("Documents du représentant", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                      ),
                    Container(
                      child: Column(
                        children: List.generate(userAttachmentState.value.attachments.length, (index) {
                          final lastCell = index == userAttachmentState.value.attachments.length - 1;
                          return AttachmentCell(state: userAttachmentState, index: index, hideLine: lastCell);
                        }),
                      ),
                    ),
                    SizedBox(height: 16),
                    Center(
                      child: CrossButton("Ajouter une pièce jointe", icon: FeatherIcons.link, onPress: () async {
                        final result = await FilePicker.platform.pickFiles(
                          type: FileType.custom,
                          allowedExtensions: ["pdf", "doc", "docx", "jpg", "png"],
                        );
                        if (result != null) {
                          final file = AttachmentFile(filePath: result.files.elementAt(0).path!, filename: result.files.elementAt(0).name);
                          final attachments = userAttachmentState.value.attachments;
                          attachments.add(file);
                          userAttachmentState.value = userAttachmentState.value.copyWith(attachments: attachments);
                        }
                      }),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
