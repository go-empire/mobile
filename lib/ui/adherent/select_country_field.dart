import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/shared/utils.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SelectCountryField extends StatelessWidget {
  final IconData icon;
  final String prefix;
  final bool hideLine;

  const SelectCountryField(
      this.icon,
      this.prefix,{
        Key? key,
        this.hideLine = false,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: Const.color.primary)
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Row(
              children: [
                Icon(icon, color: Const.color.primary),
                SizedBox(width: 25),
                Text(prefix, style: Const.style.mediumBodyText.copyWith(color: Const.color.primary)),
                Expanded(
                  child: Country(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 75),
            child: Container(
              height: hideLine ? 0 : 1,
              color: Const.color.textBody,
            ),
          )
        ],
      ),
    );
  }
}

class Country extends StatefulWidget {

  const Country({
    Key? key
  }) : super(key: key);

  @override
  _CountryState createState() => _CountryState();
}

class _CountryState extends State<Country> {
  String? flag;
  String? country;

  @override
  void initState() {
    super.initState();
    final form = context.read(adherentFormProvider);
    country = form.control('adherent.country').value;
    if (country != null) flag = countryFlag(country!);
    print(flag);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () async {
        FocusScope.of(context).requestFocus(new FocusNode());
        final countrySelected = await Navigator.pushNamed(context, Routes.selectCountry) as Map<String, String>?;
        if (countrySelected != null) {
          setState(() {
            flag = countrySelected["code"];
            country = countrySelected["name"];
            final form = context.read(adherentFormProvider);
            form.control('adherent.country').value = country ?? '';
          });
        }
      },
      child: Container(
        height: 48,
        child: flag != null
            ? Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(3),
              child: SvgPicture.asset(
                "assets/images/countries/$flag.svg",
                width: 33.w,
                height: 19.h,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Text("$country", style: Const.style.mediumBodyText.copyWith(color: Const.color.primary)),
            ),
            Icon(Icons.expand_more),
          ],
        )
            : Align(alignment: Alignment.centerRight, child: Icon(Icons.expand_more)),
      ),
    );
  }
}
