import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:empire/extensions/extensions.dart';

class AdherentList extends HookWidget {
  const AdherentList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final adherentsState = useProvider(adherentProvider.state);
    if (adherentsState.isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    if (adherentsState.hasError) {
      return Center(
        child: Column(
          children: [
            ElevatedButton(
              child: Text("Reessayer"),
              onPressed: () async {
                await context.read(adherentProvider).fetch();
              },
            )
          ],
        ),
      );
    }

    final adherents = adherentsState.adherents;
    return ListView.separated(
      itemBuilder: (context, index) {
        return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              context.read(adherentDetailsProvider).state = index;
              Navigator.pushNamed(context, Routes.adherentDetails);
            },
            child: AdherentItem(adherents!.elementAt(index)));
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 20);
      },
      itemCount: adherents!.length,
    );
  }
}

class AdherentItem extends HookWidget {
  final Adherent adherent;

  const AdherentItem(
    this.adherent, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(200),
            color: Const.color.primary,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              height: 80,
              width: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
              ),
              child:
                  adherent.logo != null ? CachedNetworkImage(imageUrl: adherent.logo!, fit: BoxFit.cover) : Container(color: Const.color.secondary),
            ),
          ),
        ),
        SizedBox(width: 20),
        Flexible(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Container(
                  child: Text(
                    "${adherent.name!.capitalize}",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                  ),
                ),
              ),
              Text("${adherent.country!.capitalize}", style: TextStyle(fontWeight: FontWeight.w100, fontSize: 14)),
              Row(
                children: [
                  Wrap(
                    children: List.generate(5, (index) {
                      return int.parse(adherent.level ?? "5") >= index
                          ? Icon(Icons.star, color: Const.color.primary, size: 14)
                          : Icon(Icons.star_border, color: Const.color.primary, size: 14);
                    }),
                  ),
                  SizedBox(width: 10),
                  Container(
                    decoration: BoxDecoration(
                      color: Const.color.secondary,
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Center(
                        child: Text("${adherent.category?.toUpperCase()}", style: Const.style.bodyText.copyWith(fontSize: 11.sp, color: Colors.white))),
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}
