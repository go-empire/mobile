import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/category.dart';
import 'package:empire/states/category_state.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CategoriesList extends StatefulWidget {
  @override
  _CategoriesListState createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: () {
          String? id = context.read(categoryIdSelectedProvider).state;
          Navigator.pop(context, id);
        }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text("Catégories", style: Const.style.pageTitle),
      ),
      body: Consumer(
        builder: (context, watch, _) {
          List<Category> categories = watch(categoriesStateNotifier.state).categories;
          String? categoryIdSelected = watch(categoryIdSelectedProvider).state;

          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListView.separated(
                shrinkWrap: true,
                itemCount: categories.length,
                separatorBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    height: .2,
                    color: Const.color.secondary.withOpacity(.5),
                  );
                },
                itemBuilder: (context, int index) {
                  Category category = categories.elementAt(index);
                  return ListTile(
                    onTap: () {
                      setState(() {
                        context.read(categoryIdSelectedProvider).state = categories.elementAt(index).id;
                      });
                    },
                    title: Text("${category.name}", style: Const.style.mediumText),
                    trailing: category.id == categoryIdSelected ? Icon(FeatherIcons.checkCircle, color: Const.color.green) : SizedBox.shrink(),
                  );
                },
              ),
              SizedBox(height: 20),
            ],
          );
        },
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(bottom: 8),
        color: Colors.white,
        child: TextButton(
          onPressed: () async {
            List<String>? result = await showTextInputDialog(
              title: "Nouvelle catégorie",
              textFields: [DialogTextField(hintText: "Nom de la catégorie")],
              context: context,
            );
            if (result != null && result.isNotEmpty) {
              context.read(categoriesStateNotifier).add(result.elementAt(0));
            }
          },
          child: Text("Ajouter une catégorie"),
        ),
      ),
    );
  }
}
