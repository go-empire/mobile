import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/adherent.dart';
import 'package:empire/models/conversation.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/chat_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:empire/ui/adherent/key_numbers.dart';
import 'package:empire/ui/adherent/representant_infos.dart';
import 'package:empire/ui/chat/admin_chat.dart';
import 'package:empire/ui/dashboard/recent_projects.dart';
import 'package:empire/ui/users/user_details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:collection/collection.dart';

import 'add_representant_page.dart';

class RepresentantsOfAdherent extends HookWidget {
  const RepresentantsOfAdherent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final adherentFuture = useProvider(adherentRepresentants);
    useProvider(conversationListProvider.state);
    return adherentFuture.when(
        data: (representants) {
          return Column(
            children: [
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Représentants"),
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, AddRepresentantPage.path);
                      },
                      child: Text("Ajouter"))
                ],
              ),
              ListView.separated(
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    final representant = representants.elementAt(index);
                    return GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        context.read(userSelectedProvider).state = representant;
                        Navigator.pushNamed(context, UserDetailsPage.path);
                      },
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), boxShadow: [
                                BoxShadow(color: Colors.black, offset: Offset(0, 10), blurRadius: 6),
                              ]),
                              child: representant.profilePictureUrl != null
                                  ? CachedNetworkImage(imageUrl: representant.profilePictureUrl!, fit: BoxFit.cover)
                                  : Container(color: Const.color.secondary),
                            ),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${representant.fullname}", style: Const.style.mediumBodyText),
                                Text("${representant.job}", style: Const.style.bodyText),
                              ],
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () async {
                              final conversations = context.read(conversationListProvider.state).conversations;
                              Conversation? conversation = conversations.firstWhereOrNull((conversation) => conversation.userId == representant.id);

                              if (conversation == null) {
                                Services.message.displayMessage("Une erreur est survenue lors de l'ouverture du chat, reéssayer plutard");
                                return;
                              }

                              context.read(conversationIdSelectedProvider).state = conversation.conversationId;
                              await Navigator.pushNamed(context, AdminChat.path);
                              context.read(conversationIdSelectedProvider).state = null;
                            },
                            child: Text("Chat"),
                          ),
                          // Column(
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   children: [
                          //     Text("${adherent.name!.capitalize}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                          //     Text("${adherent.country!.capitalize}", style: TextStyle(fontWeight: FontWeight.w100, fontSize: 14)),
                          //     Row(
                          //       children: [
                          //         Wrap(
                          //           children: List.generate(5, (index) {
                          //             return int.parse(adherent.level ?? "5") >= index
                          //                 ? Icon(Icons.star, color: Const.color.primary, size: 14)
                          //                 : Icon(Icons.star_border, color: Const.color.primary, size: 14);
                          //           }),
                          //         ),
                          //         SizedBox(width: 10),
                          //         Container(
                          //           decoration: BoxDecoration(
                          //             color: Const.color.secondary,
                          //             borderRadius: BorderRadius.circular(7),
                          //           ),
                          //           child: Center(
                          //               child: Text("${adherent.category?.toUpperCase()}", style: Const.style.bodyText.copyWith(fontSize: 11.sp, color: Colors.white))),
                          //           padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                          //         ),
                          //       ],
                          //     ),
                          //   ],
                          // )
                        ],
                      ),
                    );
                    return Text("${representant.firstname}");
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 20);
                  },
                  itemCount: representants.length)
            ],
          );
        },
        loading: () => CircularProgressIndicator(),
        error: (err, stack) => Text("Impossible de recuper les representants"));
  }
}
