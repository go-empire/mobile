import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/adherent.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/project_state.dart';
import 'package:empire/ui/dashboard/recent_projects.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:empire/extensions/extensions.dart';

class KeyNumbers extends HookWidget {
  const KeyNumbers({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final index = useProvider(adherentDetailsProvider).state;
    final adherent = useProvider(adherentProvider.state).adherents!.elementAt(index);

    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.center,
      spacing: 40,
      children: [
        SizedBox(
          width: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("${adherent.revenue?.human}", style: Const.style.mediumBodyText),
              Text(
                "CA",
                style: Const.style.thinBodyText.copyWith(fontSize: 10),
                textAlign: TextAlign.end,
              ),
            ],
          ),
        ),
        SizedBox(
          width: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Consumer(
                builder: (BuildContext context, watch, Widget? child) {
                  final projects = watch(adherentProjectsProvider).state;
                  return Text("${projects.length}", style: Const.style.mediumBodyText);
                },
              ),
              Text("Projets", style: Const.style.thinBodyText.copyWith(fontSize: 10)),
            ],
          ),
        ),
        SizedBox(
          width: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${adherent.points}", style: Const.style.mediumBodyText),
              Text("Points", style: Const.style.thinBodyText.copyWith(fontSize: 10)),
            ],
          ),
        )
      ],
    );
  }
}
