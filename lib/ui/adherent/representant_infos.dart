import 'package:empire/constants/constants.dart';
import 'package:empire/ui/adherent/select_country_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:empire/ui/shared/basic_form_field.dart';

class RepresentantInfos extends HookWidget {

  const RepresentantInfos({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final nameController = useTextEditingController();
    final revenueController = useTextEditingController();
    final phoneController = useTextEditingController();
    final countryController = useTextEditingController();
    final categoryController = useTextEditingController();
    final levelController = useTextEditingController();

    return Column(
      children: [

      ],
    );
  }
}
