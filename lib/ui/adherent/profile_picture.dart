import 'dart:io';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePicture extends StatelessWidget {
  final ValueNotifier<AttachmentState> state;

  const ProfilePicture({
    required this.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final image = state.value.attachments.isNotEmpty ? state.value.attachments.elementAt(0) : null;
    return Center(
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Builder(
            builder: (context) {
              if (image != null) {
                return Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200),
                    color: Const.color.primary
                  ),
                  padding: EdgeInsets.all(7),
                  child: CircleAvatar(
                    maxRadius: 60,
                    backgroundImage: image.filename == "network" ? CachedNetworkImageProvider(image.filePath) : Image.file(File(image.filePath)).image,
                  ),
                );
              }
              return CircleAvatar(
                maxRadius: 60,
                backgroundColor: Const.color.primary,
              );
            },
          ),
          Positioned(
            right: -20,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(200),
              ),
              padding: EdgeInsets.all(3),
              child: FloatingActionButton(
                foregroundColor: Colors.white,
                backgroundColor: Const.color.primary,
                elevation: 2,
                child: Icon(FeatherIcons.camera),
                onPressed: () async {
                  final result = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 75);
                  if (result != null) {
                    // if (image != null) {
                    //   image.filePath = result.path;
                    //   state.value = state.value.copyWith(attachments: state.value.attachments);
                    //   return;
                    // }
                    state.value = state.value.copyWith(attachments: [AttachmentFile(filePath: result.path, filename: "local")]);
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
