import 'package:empire/constants/constants.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

class SelectStar extends StatefulWidget {
  @override
  _SelectStarState createState() => _SelectStarState();
}

class _SelectStarState extends State<SelectStar> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: () {
          Navigator.pop(context);
        }),
        backgroundColor: Colors.transparent,
        title: Text("Sélection"),
      ),
      body: Consumer(
        builder: (context, watch, _) {
          final field = context.read(adherentFormProvider).control('adherent.level');

          return ListView.builder(
            itemCount: 5,
            itemBuilder: (context, int index) {
              return ListTile(
                onTap: () {
                  setState(() {
                    field.value = (index + 1).toString();
                  });
                },
                title: Row(
                  children: [
                    SizedBox(
                      width: 20,
                      child: Text("${index + 1}", style: Const.style.mediumText.copyWith(color: int.parse(field.value) - 1 == index ? Const.color.green : Const.color.textBody)),
                    ),
                    Icon(
                      FeatherIcons.star,
                      size: 14.sp,
                      color: int.parse(field.value) - 1 == index ? Const.color.green : Const.color.textBody,
                    ),
                  ],
                ),
                trailing: int.parse(field.value) - 1 == index ? Icon(FeatherIcons.checkCircle, color: Const.color.green) : SizedBox.shrink(),
              );
            },
          );
        },
      ),
    );
  }
}
