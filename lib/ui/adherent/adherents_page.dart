import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/adherent/adherent_list.dart';
import 'package:empire/ui/adherent/search_adherents_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AdherentsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.color.lightWhite,
        elevation: 0,
        centerTitle: true,
        title: Text("Adhérents".toUpperCase(), style: Const.style.pageTitle.copyWith(color: Const.color.primary)),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 700),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 15),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () async {
                    await Navigator.pushNamed(context, SearchAdherentPage.path);
                    context.read(searchTermProvider).state = "";
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 12),
                    height: 40,
                    decoration: BoxDecoration(
                      color: Const.color.primary,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Text("Rechercher", style: Const.style.bodyText.copyWith(color: Const.color.lightWhite, fontWeight: FontWeight.bold)),
                  ),
                ),
                SizedBox(height: 25),
                Expanded(child: AdherentList()),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(200),
        ),
        child: FloatingActionButton(
          heroTag: "adherents_page",
          backgroundColor: Const.color.primary,
          child: Icon(FeatherIcons.plus),
          onPressed: () {
            Navigator.pushNamed(context, Routes.addAdherent);
          },
        ),
      ),
    );
  }
}
