import 'package:cached_network_image/cached_network_image.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/models/adherent.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/adherent/key_numbers.dart';
import 'package:empire/ui/adherent/represents_of_adherent.dart';
import 'package:empire/ui/adherent/update_adherent.dart';
import 'package:empire/ui/dashboard/recent_projects.dart';
import 'package:empire/ui/points/add_points_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:empire/extensions/extensions.dart';
import 'package:url_launcher/url_launcher.dart';

class AdherentDetails extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final index = useProvider(adherentDetailsProvider).state;
    final adherent = useProvider(adherentProvider.state).adherents!.elementAt(index);

    return SafeArea(
      bottom: true,
      top: false,
      child: Scaffold(
          appBar: AppBar(
            title: Text("${adherent.name}", style: Const.style.pageTitle),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, UpdateAdherent.path);
                  },
                  child: Text("Modifier"))
            ],
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 30, left: 24, right: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 30),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: adherent.logo != null
                          ? CachedNetworkImage(imageUrl: adherent.logo!, fit: BoxFit.cover)
                          : Container(color: Const.color.secondary),
                    ),
                  ),
                ),
                SizedBox(height: 12),
                Center(child: Text("${adherent.country?.capitalize}", style: Const.style.bodyText.copyWith(fontSize: 14))),
                SizedBox(height: 26),
                KeyNumbers(),
                SizedBox(height: 32),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text("Activité", style: Const.style.mediumText),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Text("${adherent.activity}", style: Const.style.bodyText),
                    )
                  ],
                ),
                RecentProjects(),
                Flexible(
                  fit: FlexFit.loose,
                  child: RepresentantsOfAdherent(),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Ink(
                  color: Const.color.green,
                  padding: EdgeInsets.all(16),
                  child: InkWell(
                    onTap: () async {
                      if (adherent.phone == null) {
                        Services.message.displayError("Numéro de l'adhérent introuvable");
                        return;
                      }

                      String phoneUrl = "tel://${adherent.phone ?? ""}";
                      if (await canLaunch(phoneUrl)) {
                        launch(phoneUrl);
                      } else {
                        Services.message.displayError("Une erreur est survenue");
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.phone_outlined, color: Colors.white),
                        SizedBox(width: 8),
                        Text(
                          "Appeler l'adhérent",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.fade,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Ink(
                  color: Const.color.blue,
                  padding: EdgeInsets.all(16),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, AddPointsPage.path);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.add_circle_outline, color: Colors.white),
                        SizedBox(width: 8),
                        Text(
                          "Ajouter des points",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.fade,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ))),
    );
  }
}
