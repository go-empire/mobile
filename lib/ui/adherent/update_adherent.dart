import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/services/services.dart';
import 'package:empire/shared/input-formatter.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/ui/adherent/profile_picture.dart';
import 'package:empire/ui/adherent/category_field.dart';
import 'package:empire/ui/adherent/select_country_field.dart';
import 'package:empire/ui/adherent/start_selection.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:empire/ui/shared/attachment_cell.dart';
import 'package:empire/ui/shared/basic_form_field.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:empire/ui/shared/cross_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_screenutil/size_extension.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class UpdateAdherent extends StatefulWidget {
  static const path = '/adherents/update';

  @override
  State<UpdateAdherent> createState() => _UpdateAdherentState();
}

class _UpdateAdherentState extends State<UpdateAdherent> {
  final errors = [];
  late final FormGroup form;

  final profileState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));
  final adherentAttachmentState = ValueNotifier<AttachmentState>(AttachmentState(attachments: []));

  initState() {
    super.initState();
    form = context.read(adherentFormProvider);
  }

  _displayError(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(text)),
    );
  }

  _validatedAdherent(FormGroup form) {
    if (form.control('adherent').hasErrors) {
      final field = form.control('adherent').errors.keys.elementAt(0);
      var errorMessage;
      switch (field) {
        case 'name':
          errorMessage = "Le nom de l'adhérent est requis";
          break;
        case 'revenue':
          errorMessage = "Le revenu est requis et constitué de chiffres";
          break;
        case 'country':
          errorMessage = "Vous devez choisir le pays";
          break;
        case 'category':
          errorMessage = "Vous devez choisir une catégorie";
          break;
        default:
          errorMessage = "Une erreur est survenue";
      }
      _displayError(errorMessage);
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconTheme.of(context).copyWith(color: Const.color.primary),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text("Modification de l'adhérent".toUpperCase(), style: Const.style.pageTitle),
          centerTitle: true,
        ),
        bottomNavigationBar: CrossBottomButton("Modifier l'adhérent", onPress: () async {
          final form = context.read(adherentFormProvider);
          if (!_validatedAdherent(form)) return;

          Services.work.start();
          FocusScope.of(context).unfocus();

          // if filename is local, edit if not do not edit
          final profilePicture = profileState.value.attachments.isNotEmpty && profileState.value.attachments.first.filename == 'local' ? profileState.value.attachments.first : null;
          final adherentAttachments = adherentAttachmentState.value.attachments;

          final createProfileFile = profilePicture != null ? CreateFileDto.simple("profile", profilePicture.filename, profilePicture.filePath) : null;
          final createAdherentAttachments = adherentAttachments.isNotEmpty
              ? adherentAttachments.map((AttachmentFile attachment) {
                  return CreateFileDto.simple("attachment", attachment.filename, attachment.filePath);
                }).toList(growable: false)
              : null;

          final success = await context.read(adherentProvider).update(
                profilePicture: createProfileFile,
                adherentAttachments: createAdherentAttachments,
              );

          if (success) {
            Navigator.pop(context);
            Services.work.stop();
          } else {
            Services.work.stop();
          }
        }),
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 48.h, left: 16, right: 16),
          child: Consumer(
            builder: (context, watch, _) {
              final form = watch(adherentFormProvider);
              final adherent = watch(adherentSelectedProvider).state;
              final createAdherent = CreateAdherentDto(
                  name: adherent.name,
                  revenue: adherent.revenue,
                  phone: adherent.phone,
                  email: adherent.email,
                  address: adherent.address,
                  country: adherent.country,
                  categoryId: adherent.categoryId,
                  level: adherent.level,
                  activity: adherent.activity);
              form.reset(value: {
                'adherent': createAdherent.toMap(),
              });
              if (adherent.logo != null) {
                print(adherent.logo);
                profileState.value = profileState.value.copyWith(
                  attachments: [
                    AttachmentFile(filePath: adherent.logo ?? "", filename: "network"),
                  ],
                );
              }
              return ReactiveForm(
                formGroup: form,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 24),
                    ValueListenableBuilder(
                      builder: (BuildContext context, value, Widget? child) {
                        return ProfilePicture(state: profileState);
                      },
                      valueListenable: profileState,
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Entreprise", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    // adherent informations
                    Container(
                      child: Column(
                        children: [
                          BasicFormField(FeatherIcons.user, "Nom *", 'adherent.name'),
                          SizedBox(height: 12),
                          BasicFormField(
                            FeatherIcons.dollarSign,
                            "Chiffre d'affaiare",
                            "adherent.revenue",
                            inputType: TextInputType.number,
                            // inputFormatters: [SpacedNumberFormatter()],
                          ),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.phone, "Téléphone", 'adherent.phone', inputType: TextInputType.phone),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.mail, "Email", 'adherent.email', inputType: TextInputType.emailAddress),
                          SizedBox(height: 12),
                          BasicFormField(FeatherIcons.mapPin, "Adresse", 'adherent.address'),
                          SizedBox(height: 12),
                          SelectCountryField(FeatherIcons.globe, "Pays *", hideLine: true),
                        ],
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    SizedBox(height: 32),
                    Container(
                      child: Column(
                        children: [
                          CategoryField(FeatherIcons.alignJustify, "Catégorie *"),
                          SizedBox(height: 12),
                          StarSelection(FeatherIcons.star, "Niveau", hideLine: true)
                        ],
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Activités", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    Container(
                      child: ReactiveTextField(
                        formControlName: 'adherent.activity',
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                          borderSide: BorderSide(color: Const.color.primary, width: 2),
                          borderRadius: BorderRadius.circular(30),
                        )),
                        style: Const.style.mediumBodyText,
                        minLines: 8,
                        maxLines: 8,
                      ),
                    ),
                    SizedBox(height: 32),
                    if (adherentAttachmentState.value.attachments.isNotEmpty)
                      Center(
                        child: Container(
                          width: .5.sw,
                          height: 2,
                          color: Const.color.primary,
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18, top: 20),
                      child: Text("Documents", style: TextStyle(color: Const.color.darker, fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    ValueListenableBuilder(
                      builder: (BuildContext context, AttachmentState value, Widget? child) {
                        return Container(
                          color: Colors.white,
                          child: Column(
                            children: List.generate(value.attachments.length, (index) {
                              final lastCell = index == value.attachments.length - 1;
                              return AttachmentCell(state: adherentAttachmentState, index: index, hideLine: lastCell);
                            }),
                          ),
                        );
                      },
                      valueListenable: adherentAttachmentState,
                    ),
                    SizedBox(height: 16),
                    Center(
                      child: SizedBox(
                        width: double.infinity,
                        child: CrossButton("Ajouter une pièce jointe", icon: FeatherIcons.link, onPress: () async {
                          final result = await FilePicker.platform.pickFiles(
                            type: FileType.custom,
                            allowedExtensions: ["pdf", "doc", "docx", "jpg", "png"],
                          );
                          if (result != null) {
                            final file = AttachmentFile(filePath: result.files.elementAt(0).path!, filename: result.files.elementAt(0).name);
                            final attachments = adherentAttachmentState.value.attachments;
                            attachments.add(file);
                            adherentAttachmentState.value = adherentAttachmentState.value.copyWith(attachments: attachments);
                          }
                        }),
                      ),
                    ),
                    SizedBox(height: 32),
                    Center(
                      child: Container(
                        width: .5.sw,
                        height: 2,
                        color: Const.color.primary,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
