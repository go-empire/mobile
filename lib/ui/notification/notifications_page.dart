import 'package:empire/constants/constants.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/states/notifications_state.dart';
import 'package:empire/states/states.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import 'add_notification_page.dart';

class NotificationPage extends StatelessWidget {

  static const path = '/notifications';

  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final format = DateFormat("dd MMMM yyyy HH:mm");
    return Consumer(
      builder: (BuildContext context, watch, Widget? child) {
        final notificationListState = watch(notificationsProvider.state);
        final notifications = notificationListState.notifications;

        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text("Notifications", style: Theme.of(context).appBarTheme.titleTextStyle),
            actions: [
              Consumer(
                builder: (BuildContext context, watch, child) {
                  final user = watch(currentUserNotifier.state).user;

                  if (user?.isRepresentant == true) {
                    return SizedBox.shrink();
                  }

                  return TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, AddNotificationPage.path);
                    },
                    child: Text("Créer", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  );
                },
              ),
            ],
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ListView.separated(itemBuilder: (context, index) {
                final notification = notifications.elementAt(index);
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("${notification.title}", style: Const.style.mediumText.copyWith(fontWeight: FontWeight.bold)),
                          SizedBox(height: 16),
                          Text("${notification.message}"),
                          SizedBox(height: 32),
                          Text("${format.format(notification.createdAt!)}", style: Const.style.thinBodyText.copyWith(color: Const.color.primary)),
                        ],
                      ),
                    ),
                  ),
                );
              }, separatorBuilder: (context, index) {
                return SizedBox(height: 16);
              }, itemCount: notifications.length),
            ),
          ),
        );
      },
    );
  }
}
