import 'package:empire/dto/create_notification_dto.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/notifications_state.dart';
import 'package:empire/states/states.dart';
import 'package:empire/ui/shared/cross_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

class AddNotificationPage extends StatelessWidget {
  static const path = '/notifications/add';

  const AddNotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text("Ajouter une notification", style: Theme.of(context).appBarTheme.titleTextStyle),
      ),
      body: SafeArea(
        child: Consumer(
          builder: (context, watch, _) {
            return ReactiveForm(
              formGroup: watch(addNotificationForm),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ReactiveTextField(
                      formControlName: 'title',
                      decoration: InputDecoration(
                          filled: true,
                          hintText: 'Titre de la notification',
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(7),
                          )),
                    ),
                    SizedBox(height: 32),
                    ReactiveTextField(
                      formControlName: 'message',
                      maxLines: 12,
                      decoration: InputDecoration(
                        filled: true,
                        hintText: 'Message',
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: CrossBottomButton(
        'Envoyer',
        onPress: () async {
          Services.work.start();
          if (context.read(addNotificationForm).hasErrors) {
            Services.message.displayError("Vous devez remplire tous les champs");
            Services.work.stop();
            return;
          }

          final value = context.read(addNotificationForm).value;
          final createNotification = CreateNotificationDto.fromMap(value);
          bool success = await context.read(notificationsProvider).add(createNotification);
          if (success) {
            Services.work.stop();
            context.read(addNotificationForm).reset(value: { "title": '', "message": '' });
            Navigator.pop(context);
          } else {
            Services.work.stop();
          }
        },
      ),
    );
  }
}
