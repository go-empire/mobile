import "dart:convert";

import 'package:empire/constants/constants.dart';
import 'package:empire/models/auth.dart';
import 'package:empire/models/models.dart';
import 'package:empire/routes/routes.dart';
import 'package:empire/services/http_service.dart';
import 'package:empire/services/services.dart';
import 'package:flutter/material.dart';
import "package:shared_preferences/shared_preferences.dart";

class AuthService {

  Future<bool> get isUserAuthenticated async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(Const.sp.authKey);
  }

  Future<Auth?> getAuth() async {
    final prefs = await SharedPreferences.getInstance();
    final authString = prefs.getString(Const.sp.authKey);
    if (authString == null) return null;
    return Auth.fromMap(json.decode(authString));
  }

  setAuth(Auth? auth) async {
    final sp = await SharedPreferences.getInstance();
    if (auth != null) {
      final authMap = auth.toMap();
      sp.setString(Const.sp.authKey, json.encode(authMap));
    } else {
      sp.remove(Const.sp.authKey);
    }
  }

  Future<Auth?> signIn(String email, String password) async {
    try {
      final result = await Services.api.post(Const.api.signIn, {"email": email, "password": password});
      Auth auth = Auth.fromMap(result);
      setAuth(auth);
      return auth;
    } catch (ex) {
      return null;
    }
  }

  Future<Auth?> resetPassword(String email, String defaultPassword, String newPassword) async {
    try {
      final result = await Services.api.post(Const.api.resetPassword, {"email": email, "password": defaultPassword, "newPassword": newPassword});
      Auth auth = Auth.fromMap(result);
      setAuth(auth);
      return auth;
    } catch (ex) {
      return null;
    }
  }

  Future<Auth> signUp(String email, String password) async {
    final result = await Services.api.post(Const.api.signUp, {"email": email, "password": password});
    Auth auth = Auth.fromMap(result);
    setAuth(auth);
    return auth;
  }

  Future signOut() async {
    setAuth(null);
  }
}
