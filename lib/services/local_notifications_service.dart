import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:math';

class LocalNotificationsService {
  final FlutterLocalNotificationsPlugin _localNotifications = FlutterLocalNotificationsPlugin();
  final rand = Random();

  static final LocalNotificationsService _notificationService = LocalNotificationsService._internal();

  factory LocalNotificationsService() {
    return _notificationService;
  }

  LocalNotificationsService._internal();

  init() async {
    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('logo');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(onDidReceiveLocalNotification: _onDidReceiveLocalNotification);

    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: null,
    );

    await _localNotifications.initialize(initializationSettings, onSelectNotification: _onLocalNotificationSelected);
  }

  displayNotification(String? id, String? title, String? message) async {
    AndroidNotificationDetails androidSpecs = AndroidNotificationDetails(
      id ?? rand.nextInt(10000).toString(),
      'Empire',
      channelDescription: 'Empire message',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
    );
    IOSNotificationDetails iosSpecs = IOSNotificationDetails();
    NotificationDetails specs = NotificationDetails(android: androidSpecs, iOS: iosSpecs);
    await _localNotifications.show(0, title, message, specs);
  }

  _onLocalNotificationSelected(String? payload) {}

  _onDidReceiveLocalNotification(int notificationId, String? title, String? body, String? payload) {}
}
