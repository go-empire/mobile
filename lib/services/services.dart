import 'package:empire/services/errors_handler_service.dart';
import 'package:empire/services/local_notifications_service.dart';
import 'package:empire/services/push_service.dart';
import 'package:empire/services/socketio_service.dart';
import 'package:empire/services/upload_service.dart';

import 'api_service.dart';
import 'auth_service.dart';
import 'work_service.dart';

class Services {
  static final auth = AuthService();
  static final api = Api();
  static final work = WorkService.instance;
  static final upload = UploadService();
  static final message = ErrorsHandlerService();
  static final io = SocketioService.instance;
  static final push = PushService.instance;
  static final localNotification = LocalNotificationsService();
}