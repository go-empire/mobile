import 'package:empire/constants/constants.dart';
import 'package:empire/services/services.dart';
import 'package:empire/services/work_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketioService {
  static SocketioService? _instance;
  late Socket _socket;

  SocketioService() {
    _socket = io(Const.api.baseUrl,  OptionBuilder()
        .setTransports(['websocket']).build());
    _loadListeners();
  }

  static SocketioService get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = SocketioService();
    return _instance!;
  }

  send(String channel, dynamic data) {
    _socket.emit(channel, data);
  }

  on(String channel, dynamic cb) {
    _socket.on(channel, cb);
  }

  _loadListeners() {
    _socket.onConnect((_) {
      print('connect');
      _socket.emit('msg', 'test');
    });


    _socket.on('event', (data) => print(data));
    _socket.on('connect_error', (data) => print(data));
    _socket.on('connect_timeout', (data) => print(data));
    _socket.on('connecting', (data) => print(data));
    _socket.on('disconnect', (data) => print(data));
    _socket.on('error', (data) => print(data));
    _socket.on('connect', (data) => print(data));
    _socket.onDisconnect((_) => print('disconnect'));
    _socket.on('fromServer', (_) => print(_));
  }
}