import 'dart:async';

import 'package:empire/models/task.dart';

class WorkService {

  final _stream = StreamController<Task>();
  static WorkService? _instance;

  static WorkService get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = WorkService();
    return _instance!;
  }

  WorkService() {
    _stream.add(Task(false, ""));
  }

  get stream => _stream.stream;

  void start({ String message = "" }) {
    _stream.add(Task(true, message));
  }

  void stop() {
    _stream.add(Task(false, ""));
  }

  void close() {
    _stream.close();
  }
}