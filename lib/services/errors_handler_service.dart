import 'package:empire/services/work_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ErrorsHandlerService {

  static ErrorsHandlerService? _instance;

  static ErrorsHandlerService get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = ErrorsHandlerService();
    return _instance!;
  }

  displayError(String message) {
    WorkService.instance.stop();
    Fluttertoast.showToast(msg: message, backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
  }

  displayMessage(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_LONG);
  }
}