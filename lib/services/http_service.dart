import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/services/services.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HttpService {
  static Dio? _dio;

  static Dio get tokenDio {
    final dio = new Dio();
    dio.options.baseUrl = Const.api.baseUrl;
    return dio;
  }

  Dio get dio {
    if (_dio == null) {
      _dio = new Dio();
      _dio?.options.baseUrl = Const.api.baseUrl;
      _dio?.options.connectTimeout = 5000; //5s
      _dio?.options.receiveTimeout = 3000;
      _dio?.interceptors.add(
        InterceptorsWrapper(
          onRequest: requestInterceptor,
          onResponse: responseInterceptor,
          onError: errorInterceptor,
        ),
      );
    }

    return _dio!;
  }

  requestInterceptor(RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers["Accept"] = "application/json";
    options.responseType = ResponseType.json;
    print(options.uri);
    final auth = await Services.auth.getAuth();
    if (auth != null) {
      options.headers["Authorization"] = "Bearer " + auth.token;
      print("request: ${options.method}: ${options.path}");
    }
    return handler.next(options);
  }

  responseInterceptor(Response response, ResponseInterceptorHandler handler) {
    return handler.next(response);
  }

  errorInterceptor(DioError error, ErrorInterceptorHandler handler) async {
    _handleError(error);
    return handler.next(error);
  }

  _handleError(DioError error) {
    switch (error.type) {
      case DioErrorType.receiveTimeout:
        Fluttertoast.showToast(msg: "The request did not reach the server, maybe network is slow", backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
        break;
      case DioErrorType.response:
        if (error.response?.statusCode == 404) {
          Fluttertoast.showToast(msg: json.encode(error.message), backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
          break;
        } else {
          final data = error.response?.data;
          print("Error code : ${data["code"]}");
          print("Error name : ${data["name"]}");
          print("Error message : ${data["message"]}");
          switch (data["code"]) {
            case 100:
              Fluttertoast.showToast(msg: "Your session have expired", backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
              break;
            case 101:
              Fluttertoast.showToast(msg: "An error occurred on server", backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
              break;
            case 110:
              Fluttertoast.showToast(msg: data["message"], backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
              break;
            default:
              Fluttertoast.showToast(msg: data["message"], backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
              break;
          }
          break;
        }
        break;
      case DioErrorType.cancel:
        final data = error.response?.data;
        print("Error code : ${data["code"]}");
        print("Error name : ${data["name"]}");
        print("Error message : ${data["message"]}");
        break;
      default:
        print(error.error);
        print(json.encode(error.error));
        Fluttertoast.showToast(msg: json.encode(error.message), backgroundColor: Colors.red, toastLength: Toast.LENGTH_LONG);
    }
  }
}
