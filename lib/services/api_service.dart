import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/models/fichier.dart';
import 'package:empire/services/http_service.dart';
import 'package:empire/services/work_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:synchronized/extension.dart';

class Api {
  get(String url) async {
    try {
      final response = await HttpService().dio.get("$url");
      if (response.statusCode == 200) {
        return response.data;
      } else {
        return null;
      }
    } on SocketException {
      return null;
    } on DioError catch (e) {
      _handleHttpError(e);
      return null;
    }
  }

  post(String url, [dynamic body]) async {
    try {
      final response = await HttpService().dio.post("$url", data: body);
      if (response.statusCode == 200) {
        return response.data;
      }
      return null;
    } on SocketException {
      return null;
    } on DioError catch (e) {
      _handleHttpError(e);
      return null;
    }
  }

  patch(String url, [dynamic body]) async {
    try {
      final response = await HttpService().dio.patch("$url", data: body);
      if (response.statusCode == 200) {
        return response.data;
      }
      return null;
    } on SocketException {
      return null;
    } on DioError catch (e) {
      _handleHttpError(e);
      return null;
    }
  }

  delete(String url) async {
    try {
      final response = await HttpService().dio.delete("$url");
      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 500) {
        throw Exception('Une erreur est survenue, veuillez reésayer plustard');
      } else {
        throw Exception(json.decode(response.data));
      }
    } on SocketException {
      throw Exception("Pas de connection internet");
    }
  }

  Future<Fichier?> uploadFile(String filePath, CreateFileDto data, [ProgressCallback? onSendProgress, ProgressCallback? onReceiveProgress]) async {
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(filePath),
      ...data.toMap(),
    });
    try {
      final response = await HttpService().dio.post(
            "${Const.api.uploadFile}",
            data: formData,
            options: Options(sendTimeout: 60 * 1000 * 2, extra: {"file": filePath}),
            onSendProgress: onSendProgress,
            onReceiveProgress: onReceiveProgress,
          );
      if (response.statusCode == 200) {
        return Fichier.fromMap(response.data);
      } else {
        WorkService.instance.stop();
        return null;
      }
    } catch(e) {
      print(e);
      return null;
    }
  }

  uploadFiles(String url, List<String> paths) async {
    FormData formData = FormData.fromMap({"files": paths.map((path) async => await MultipartFile.fromFile(path))});
    try {
      final response = await HttpService().dio.post("$url", data: formData);
      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 500) {
        throw Exception('Une erreur est survenue, veuillez reésayer plustard');
      } else {
        throw Exception(json.decode(response.data));
      }
    } on SocketException {
      throw Exception("Pas de connection internet");
    } on DioError catch (e) {
      throw _handleHttpError(e);
    }
  }

  _handleHttpError(DioError error) {
    switch (error.type) {
      case DioErrorType.receiveTimeout:
        Fluttertoast.showToast(msg: "The request did not reach the server, maybe network is slow", backgroundColor: Colors.red);
        break;
      case DioErrorType.response:
        final data = error.response?.data?.toString();
        Fluttertoast.showToast(msg: data ?? "", backgroundColor: Colors.red);
        break;
      case DioErrorType.cancel:
        Fluttertoast.showToast(msg: json.encode(error.message), backgroundColor: Colors.red);
        print(error.message);
        break;
      default:
        Fluttertoast.showToast(msg: json.encode(error.message), backgroundColor: Colors.red);
        print(error);
    }
  }
}
