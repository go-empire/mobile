import 'package:empire/services/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

class PushService {
  static PushService? _instance;
  FirebaseMessaging _messaging = FirebaseMessaging.instance;

  PushService() {
    _loadListeners();
  }

  static PushService get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = PushService();
    return _instance!;
  }

  _loadListeners() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        Services.localNotification.displayNotification("admin-notification", message.notification?.title, message.notification?.body);
      }
    });
  }

  subscribeToTopic(String topic) {
    debugPrint('subscribe to topic: $topic');
    _messaging.subscribeToTopic(topic);
  }
}

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");
  print("Handling a background message: ${message.data}");
  print("Handling a background message: ${message.category}");
  print("Handling a background message: ${message.collapseKey}");
  print("Handling a background message: ${message.notification?.title}");
  print("Handling a background message: ${message.notification?.body}");
  print("Handling a background message: ${message.notification?.android?.priority}");
  print("Handling a background message: ${message.notification?.apple?.subtitle}");
}