import 'package:empire/models/article.dart';
import 'package:empire/models/category.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/states/base_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final categoriesStateNotifier = StateNotifierProvider((_) => CategoriesNotifier(CategoryListState(categories: [])..loading()));
final categoryIdSelectedProvider = StateProvider<String?>((_) => null);

class CategoryListState extends BaseState {
  late List<Category> categories;

  CategoryListState({required this.categories});

  CategoryListState copyWith({List<Category>? categories}) {
    return new CategoryListState(categories: categories ?? this.categories);
  }
}

class CategoriesNotifier extends StateNotifier<CategoryListState> {
  CategoriesNotifier(CategoryListState state) : super(state) {
    fetch();
  }

  fetch() async {
    final categories = await Repositories.category.fetch();
    if (categories == null) {
      state = state.copyWith()..error("Error getting articles");
      return;
    }

    state = state.copyWith(categories: categories);
  }

  add(String name) async {
    Category? category = await Repositories.category.create(name);
    if (category == null) {
      return;
    }

    state = state.copyWith(categories: [...state.categories, category]);
  }

  update(String id, String name) async {
    Category? category = await Repositories.category.update(id, name);
    if (category == null) {
      return;
    }

    state = state.copyWith(categories: state.categories.map((c) => c.id == id ? category : c).toList());
  }
}
