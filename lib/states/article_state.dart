import 'package:empire/models/article.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/states/base_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final articleListProvider = StateNotifierProvider((_) => ArticleListNotifier(ArticleListState(articles: [])..loading()));
final articleSelectedProvider = StateProvider<int>((_) => -1);

class ArticleListState extends BaseState {
  late List<Article> articles;

  ArticleListState({required this.articles});

  ArticleListState copyWith({List<Article>? articles}) {
    return new ArticleListState(articles: articles ?? this.articles);
  }
}

class ArticleListNotifier extends StateNotifier<ArticleListState> {
  ArticleListNotifier(ArticleListState state) : super(state) {
    fetch();
  }

  fetch() async {
    final articles = await Repositories.article.fetch();
    if (articles == null) {
      state = state.copyWith()..error("Error getting articles");
      return;
    }

    state = state.copyWith(articles: articles);
  }
}
