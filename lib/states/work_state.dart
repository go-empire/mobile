import 'package:empire/models/task.dart';
import 'package:empire/services/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final workProvider = StreamProvider<Task>((ref) {
  return Services.work.stream;
});