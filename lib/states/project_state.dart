import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/dto/create_project_dto.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/services/services.dart';
import 'package:empire/shared/form_validators.dart';
import 'package:empire/states/adherent_state.dart';
import 'package:empire/states/base_state.dart';
import 'package:empire/models/models.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:collection/collection.dart';

final projectFormProvider = Provider<FormGroup>((_) {
  return fb.group({
    'title': ['', Validators.required],
    'description': ['', Validators.required],
    'budget': ['', Validators.number],
    'startDate': ['', CustomValidator.isDate],
    'duration': ['', Validators.number],
  });
});

final projectListNotifier = StateNotifierProvider<ProjectNotifier>((ref) => ProjectNotifier(ProjectState(projects: [])..loading(), ref));
final searchProjectTermProvider = StateProvider<String>((_) => "");
final projectsFilteredProvider = StateProvider<List<Project>>((ref) {
  final searchTerm = ref.watch(searchProjectTermProvider).state;
  if (searchTerm.isEmpty) return [];

  final projects = ref.watch(projectListNotifier.state).projects;
  return projects.where((project) => (project.title?.toLowerCase().indexOf(searchTerm.toLowerCase().trim()) ?? -1) >= 0).toList();
});
final projectSelectedIdProvider = StateProvider<String?>((_) => null);
final projectSelectedProvider = StateProvider<Project?>((ref) {
  final id = ref.watch(projectSelectedIdProvider).state;
  final projects = ref.watch(projectListNotifier.state).projects;
  return projects.singleWhereOrNull((p) => p.id == id);
});
final adherentProjectsProvider = StateProvider<List<Project>>((ref) {
  final adherentSelected = ref.watch(adherentSelectedProvider).state;
  final projects = ref.watch(projectListNotifier.state).projects;

  return projects.where((project) => project.adherentId == adherentSelected.id).toList();
});

class ProjectState extends BaseState {
  late List<Project> projects;

  ProjectState({required this.projects});

  ProjectState copyWith({List<Project>? projects}) {
    return new ProjectState(projects: projects ?? this.projects);
  }
}

class ProjectNotifier extends StateNotifier<ProjectState> {
  late final ProviderReference ref;

  ProjectNotifier(ProjectState state, ProviderReference ref) : super(state) {
    fetch();
    this.ref = ref;
  }

  fetch() async {
    List<Project>? projects = await Repositories.project.fetch();
    if (projects == null) return;

    projects.sort((Project p1, Project p2) => p2.updatedAt?.compareTo(p1.updatedAt ?? DateTime.now()) ?? 1);
    state = ProjectState(projects: projects);
  }

  Future<bool> create(List<CreateFileDto> attachments) async {
    final values = ref.read(projectFormProvider).value;
    CreateProjectDto createProjectDto = CreateProjectDto.fromMap(values);
    Project? project = await Repositories.project.create(createProjectDto);

    if (project == null) return false;

    attachments.forEach((CreateFileDto attachment) async {
      attachment.owner = project.id;
      await Services.api.uploadFile(attachment.path, attachment);
    });

    state = state.copyWith(projects: [project, ...state.projects]);
    return true;
  }

  Future<void> updateStatus(String id, int status) async {
    Project? project = await Repositories.project.update(id, { "status": status });
    if (project == null) return;

    state = state.copyWith(projects: state.projects.map((p) => p.id == project.id ? project : p).toList());
  }
}
