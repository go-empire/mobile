import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/services/socketio_service.dart';
import 'package:empire/states/user_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'chat_state.dart';

final localNotificationsProvider = StateNotifierProvider<LocalNotificationNotifier>((ref) {
  return LocalNotificationNotifier(ref);
});

class LocalNotificationNotifier extends StateNotifier<int> {
  ProviderReference ref;
  SocketioService io = Services.io;

  LocalNotificationNotifier(this.ref) : super(0);

  init() {
    io.on('message', (dynamic data) {
      Message message = Message.fromMap(data);
      User user = ref.read(currentUserNotifier.state).user as User;
      if (message.senderId == user.id) return;

      if (message.receiverId == null) {
        // we are an admin and we received the message from representant
        //todo I have to check here if the current admin added this conversation or not
        Services.localNotification.displayNotification(user.id, user.fullname ?? user.firstname, message.text);
      } else if (message.receiverId == user.id) {
        // we are a representant and we received a message from admin
        Services.localNotification.displayNotification(user.id, user.fullname ?? user.firstname, message.text);
      }
    });
  }
}
