import 'package:empire/dto/create_message_dto.dart';
import 'package:empire/models/article.dart';
import 'package:empire/models/conversation.dart';
import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/services/services.dart';
import 'package:empire/services/socketio_service.dart';
import 'package:empire/states/base_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:collection/collection.dart';

final conversationListProvider = StateNotifierProvider(
  (_) => ConversationListNotifier(ConversationListState(conversations: [])..loading()),
);
final chatProvider = StateNotifierProvider.family<ChatNotifier, String>((ref, conversationId) {
  return ChatNotifier(ChatState(messages: [])..loading(), conversationId, ref);
});
final conversationIdSelectedProvider = StateProvider<String?>((_) => null);
final conversationSelectedProvider = StateProvider<Conversation?>((ref) {
  final id = ref.watch(conversationIdSelectedProvider).state;
  if (id == null) return null;
  final conversations = ref.read(conversationListProvider.state).conversations;
  return conversations.firstWhereOrNull((conversation) => conversation.conversationId == id);
});
final savedTextFieldText = StateProvider.family<String, String>((_, conversationId) => "");
final lastDateProvider = StateProvider<String?>((_) => null);
String? lastDate;

class ConversationListState extends BaseState {
  late List<Conversation> conversations;

  ConversationListState({required this.conversations});

  ConversationListState copyWith({
    List<Conversation>? conversations,
  }) {
    return ConversationListState(
      conversations: conversations ?? this.conversations,
    );
  }
}

class ChatState extends BaseState {
  List<Message> messages;
  Conversation? conversation;
  String savedTextFieldText = "";

  ChatState({
    required this.messages,
    this.conversation,
    this.savedTextFieldText = "",
  });

  ChatState copyWith({
    List<Message>? messages,
    Conversation? conversation,
    String? savedTextFieldText,
  }) {
    return ChatState(
      messages: messages ?? this.messages,
      conversation: conversation ?? this.conversation,
      savedTextFieldText: savedTextFieldText ?? this.savedTextFieldText,
    );
  }
}

class ChatNotifier extends StateNotifier<ChatState> {
  ProviderReference ref;
  String? conversationId;
  SocketioService io = Services.io;

  ChatNotifier(ChatState state, this.conversationId, this.ref) : super(state) {
    fetch();
    onMessage();
  }

  fetch() async {
    if (conversationId == null) {
      User user = ref.read(currentUserNotifier.state).user as User;
      Conversation? conversation = await Repositories.conversation.userConversation(user.id!);
      if (conversation != null) {
        List<Message>? messages = await Repositories.conversation.fetchMessages(conversation.conversationId!);
        if (messages == null) return;

        state = state.copyWith(messages: messages, conversation: conversation);
        Services.push.subscribeToTopic("chat-${conversation.conversationId}");
      } else {
        state = state.copyWith(messages: []);
      }
    } else {
      List<Message>? messages = await Repositories.conversation.fetchMessages(conversationId!);
      if (messages == null) return;

      Conversation? conversationSelected = ref.read(conversationSelectedProvider).state;
      state = state.copyWith(messages: messages, conversation: conversationSelected);
      Services.push.subscribeToTopic("chat-$conversationId");
    }
  }

  onMessage() {
    io.on('message', (dynamic data) {
      final message = Message.fromMap(data);
      User user = ref.read(currentUserNotifier.state).user as User;
      if (message.senderId == user.id) {
        // replace existing local message
        state = state.copyWith(messages: state.messages.map((Message m) => m.id == message.id ? message : m).toList());
      } else {
        state = state.copyWith(messages: [message, ...state.messages]);
      }
    });
  }

  sendMessage(CreateMessageDto message) {
    io.send('message', message.toMap());
    Message m = Message(
      text: message.text,
      type: message.text,
      senderId: message.senderId,
      receiverId: message.receiverId,
      conversationId: message.conversationId,
      createdAt: message.createdAt,
    );
    state = state.copyWith(messages: [m, ...state.messages]);
  }

  saveText(String text) {
    state = state.copyWith(savedTextFieldText: text);
  }
}

class ConversationListNotifier extends StateNotifier<ConversationListState> {
  ConversationListNotifier(ConversationListState state) : super(state) {
    fetch();
  }

  addConversation(Conversation conversation) {
    state = state.copyWith(conversations: [conversation, ...state.conversations]);
  }

  fetch() async {
    final conversations = await Repositories.conversation.fetch();
    if (conversations == null) {
      state = state.copyWith()..error("Error getting articles");
      return;
    }

    state = state.copyWith(conversations: conversations);
  }
}
