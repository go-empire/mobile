import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_adherent_dto.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/dto/create_representant_dto.dart';
import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/services/services.dart';
import 'package:empire/shared/form_validators.dart';
import 'package:empire/states/base_state.dart';
import 'package:empire/states/representant_state.dart';
import 'package:empire/states/user_state.dart';
import 'package:empire/ui/shared/AttachmentController.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:collection/collection.dart';

final adherentFormProvider = Provider<FormGroup>((_) {
  return fb.group({
    'adherent': fb.group({
      'name': FormControl<String>(validators: [Validators.required]),
      'revenue': FormControl<int>(validators: [Validators.number]),
      'phone': FormControl<String>(),
      'email': FormControl<String>(),
      'address': FormControl<String>(),
      'country': FormControl<String>(validators: [Validators.required]),
      'categoryId': FormControl<String>(validators: [Validators.required]),
      'level': FormControl<String>(value: '5'),
      'activity': FormControl<String>(),
    }),
    'representant': fb.group({
      'fullname': FormControl<String>(validators: [Validators.required]),
      'phone': FormControl<String>(),
      'email': FormControl<String>(validators: [Validators.required, Validators.email]),
      'job': FormControl<String>(),
    })
  });
});
final adherentProvider = StateNotifierProvider((ref) => AdherentNotifier(AdherentState(adherents: [])..loading(), ref));
final adherentSelectedProvider = StateProvider<Adherent>((ref) {
  final index = ref.watch(adherentDetailsProvider).state;
  final adherents = ref.watch(adherentProvider.state).adherents;

  return adherents!.elementAt(index);
});
final adherentDetailsProvider = StateProvider<int>((_) => -1);
final adherentRepresentants = FutureProvider<List<User>>((ref) async {
  final adherents = ref.watch(adherentProvider.state).adherents!;
  if (adherents.isEmpty) return [];

  final adherentSelectedIndex = ref.watch(adherentDetailsProvider).state;
  final adherent = adherents.elementAt(adherentSelectedIndex);
  final representants = await Repositories.adherent.representants(adherent.id!);
  return representants != null ? representants : [];
});
final currentUserAdherentProvider = StateProvider<Adherent?>((ref) {
  UserState userState = ref.watch(currentUserNotifier.state);
  final adherents = ref.watch(adherentProvider.state).adherents;

  return adherents?.singleWhereOrNull((adherent) => adherent.id == userState.user?.adherentId);
});
final searchTermProvider = StateProvider<String>((_) => "");
final adherentsFilteredProvider = StateProvider<List<Adherent>>((ref) {
  final searchTerm = ref.watch(searchTermProvider).state;
  if (searchTerm.isEmpty) return [];

  final adherents = ref.watch(adherentProvider.state).adherents;
  return adherents?.where((adherent) => (adherent.name?.toLowerCase().indexOf(searchTerm.toLowerCase().trim()) ?? -1) >= 0).toList() ?? [];
});

class AdherentState extends BaseState {
  List<Adherent>? adherents;

  AdherentState({this.adherents});

  AdherentState copyWith({List<Adherent>? adherents}) {
    return new AdherentState(adherents: adherents ?? this.adherents);
  }
}

class AdherentNotifier extends StateNotifier<AdherentState> {
  final ProviderReference ref;

  AdherentNotifier(AdherentState state, this.ref) : super(state) {
    fetch();
  }

  fetch() async {
    final adherents = await Repositories.adherent.fetch();
    if (adherents == null) return;
    state = state.copyWith(adherents: adherents);
  }

  Future<bool> create({CreateFileDto? profilePicture, List<CreateFileDto>? adherentAttachments, List<CreateFileDto>? representantAttachments}) async {
    try {
      Fichier? file = profilePicture != null ? await Services.api.uploadFile(profilePicture.path, profilePicture) : null;

      final form = ref.read(adherentFormProvider);
      CreateAdherentDto createAdherentDto = CreateAdherentDto.fromMap(form.control('adherent').value);

      createAdherentDto.logoId = file?.id;
      Adherent? adherent = await Repositories.adherent.create(createAdherentDto);
      if (adherent == null) return false;

      CreateRepresentantDto createRepresentantDto = CreateRepresentantDto.fromMap(form.control('representant').value);
      createRepresentantDto.adherentId = adherent.id;
      User? representant = await Repositories.adherent.createRepresentant(createRepresentantDto);
      if (representant == null) return false;

      if (adherentAttachments != null) {
        adherentAttachments.forEach((attachment) async {
          attachment.owner = adherent.id;
          await Services.api.uploadFile(attachment.path, attachment);
        });
      }

      if (representantAttachments != null) {
        representantAttachments.forEach((attachment) async {
          attachment.owner = representant.id;
          await Services.api.uploadFile(attachment.path, attachment);
        });
      }

      state = state.copyWith(adherents: [adherent, ...?state.adherents]);
      form.reset(value: {
        "adherent": {"level": "5"}
      });
      return true;
    } catch (e) {
      Services.message.displayError(e.toString());
      return false;
    }
  }

  Future<bool> update({CreateFileDto? profilePicture, List<CreateFileDto>? adherentAttachments}) async {
    try {
      final index = ref.read(adherentDetailsProvider).state;
      final adherent = state.adherents!.elementAt(index);
      Fichier? file = profilePicture != null ? await Services.api.uploadFile(profilePicture.path, profilePicture) : null;

      final form = ref.read(adherentFormProvider);
      CreateAdherentDto createAdherentDto = CreateAdherentDto.fromMap(form.control('adherent').value);

      createAdherentDto.logoId = file != null ? file.id : adherent.logoId;
      Adherent? adherentUpdated = await Repositories.adherent.update(adherent.id!, createAdherentDto);
      if (adherentUpdated == null) return false;

      if (adherentAttachments != null) {
        adherentAttachments.forEach((attachment) async {
          attachment.owner = adherent.id;
          await Services.api.uploadFile(attachment.path, attachment);
        });
      }

      final adherents = state.adherents?.map((adherent) => adherent.id == adherentUpdated.id ? adherentUpdated : adherent).toList();
      state = state.copyWith(adherents: adherents);
      form.reset(value: {
        "adherent": {"level": "5"}
      });
      return true;
    } catch (e) {
      Services.message.displayError(e.toString());
      return false;
    }
  }

  add(Adherent adherent) {
    state = state.copyWith(adherents: [adherent, ...state.adherents!]);
  }

  update2(Adherent adherent) {
    state = state.copyWith(adherents: [adherent, ...state.adherents!]);
  }

  addPoints(String adherentId, int points) async {
    Adherent? adherentUpdated = await Repositories.adherent.addPoints(adherentId, points);
    if (adherentUpdated == null) return;

    final adherents = state.adherents?.map((adherent) => adherent.id == adherentUpdated.id ? adherentUpdated : adherent).toList();
    state = state.copyWith(adherents: adherents);
  }

  Future<bool> createRepresentant(String adherentId, List<CreateFileDto>? attachments, CreateFileDto? profilePicture) async {
    final form = ref.read(representantFormProvider);
    final createRepresentantDto = CreateRepresentantDto.fromMap(form.value);
    createRepresentantDto.adherentId = adherentId;

    final representant = await Repositories.adherent.createRepresentant(createRepresentantDto);
    if (representant == null) return false;

    Fichier? file = profilePicture != null ? await Services.api.uploadFile(profilePicture.path, profilePicture) : null;
    await Repositories.user.update(representant.id!, {"profilePictureId": file?.id});

    if (attachments != null) {
      attachments.forEach((attachment) async {
        attachment.owner = representant.id;
        await Services.api.uploadFile(attachment.path, attachment);
      });
    }

    state = state.copyWith(adherents: state.adherents);
    return true;
  }
}
