import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/states/base_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

final representantFormProvider = Provider<FormGroup>((_) {
  return fb.group({
    'fullname': ['', Validators.required],
    'phone': [''],
    'email': ['', Validators.required, Validators.email],
    'job': ['']
  });
});

final representantStateProvider = StateNotifierProvider((_) => RepresentantNotifier(RepresentantState(representants: [])));
final searchRepresentantTermProvider = StateProvider<String>((_) => "");
final searchRepresentantsProvider = StateProvider<List<User>>((ref) {
  final searchTerm = ref.watch(searchRepresentantTermProvider).state;
  final representants = ref.watch(representantStateProvider.state).representants;

  if (searchTerm.isEmpty) return representants ?? [];
  return representants
      ?.where((user) => "${user.fullname}${user.firstname}${user.lastname}".toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0 && (user.isRepresentant ?? false))
      .toList() ?? [];
});

class RepresentantState extends BaseState {
  List<User>? representants;

  RepresentantState copyWith({List<User>? representants}) {
    return RepresentantState(representants: representants ?? this.representants);
  }

  RepresentantState({this.representants});
}

class RepresentantNotifier extends StateNotifier<RepresentantState> {
  RepresentantNotifier(RepresentantState state) : super(state) {
    _fetch();
  }

  _fetch() async {
    List<User>? representants = await Repositories.representant.fetch();

    if (representants == null) return;
    state = state.copyWith(representants: representants);
  }
}
