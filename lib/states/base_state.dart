import 'package:flutter/foundation.dart';

abstract class BaseState {
  bool _isLoading = false;
  bool _hasError = false;
  late Exception _exception;

  bool get isLoading => _isLoading;
  bool get hasError => _hasError;
  Exception get exception => _exception;

  loading() {
    _isLoading = true;
  }

  error(String error) {
    _exception = Exception(error);
    _hasError = true;
  }
}
