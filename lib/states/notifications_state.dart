import 'package:empire/dto/create_notification_dto.dart';
import 'package:empire/models/article.dart';
import 'package:empire/models/models.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/states/base_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';

final notificationsProvider = StateNotifierProvider((_) => NotificationListNotifier(NotificationListState(notifications: [])..loading()));
final articleSelectedProvider = StateProvider<int>((_) => -1);
final addNotificationForm = Provider((_) {
  return fb.group({
    'title': [Validators.required],
    'message': [Validators.required],
  });
});

class NotificationListState extends BaseState {
  late List<Notification> notifications;

  NotificationListState({required this.notifications});

  NotificationListState copyWith({List<Notification>? notifications}) {
    return new NotificationListState(notifications: notifications ?? this.notifications);
  }
}

class NotificationListNotifier extends StateNotifier<NotificationListState> {
  NotificationListNotifier(NotificationListState state) : super(state) {
    fetch();
  }

  fetch() async {
    final notifications = await Repositories.notifications.fetch();
    if (notifications == null) {
      state = state.copyWith()..error("Error getting articles");
      return;
    }

    state = state.copyWith(notifications: notifications);
  }

  Future<bool> add(CreateNotificationDto createNotification) async {
    final notification = await Repositories.notifications.create(createNotification);
    if (notification == null) return false;

    state = state.copyWith(notifications: [notification, ...state.notifications]);
    return true;
  }
}
