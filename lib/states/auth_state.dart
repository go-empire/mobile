import 'package:empire/constants/constants.dart';
import 'package:empire/models/models.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/user_state.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AuthState  {
  TextEditingController? email;
  TextEditingController? password;
  bool? shouldCheckEmail;
  bool? shouldResetPassword;
  TextEditingController? defaultPassword;
  TextEditingController? confirmPassword;
  bool? authenticated;
  late bool signedOut;

  AuthState.initial() {
    shouldCheckEmail = true;
    authenticated = false;
    signedOut = false;
    email = TextEditingController(text: "");
    password = TextEditingController(text: "");
    defaultPassword = TextEditingController(text: "");
    confirmPassword = TextEditingController(text: "");
  }

  AuthState({
    required this.email,
    required this.password,
    required this.shouldCheckEmail,
    required this.shouldResetPassword,
    required this.defaultPassword,
    required this.authenticated,
    required this.confirmPassword,
    required this.signedOut,
  });

  AuthState copyWith({
    TextEditingController? email,
    TextEditingController? password,
    bool? shouldCheckEmail,
    bool? shouldResetPassword,
    TextEditingController? defaultPassword,
    TextEditingController? confirmPassword,
    bool? authenticated,
    bool? signedOut,
  }) {
    return new AuthState(
      email: email ?? this.email,
      password: password ?? this.password,
      shouldCheckEmail: shouldCheckEmail ?? this.shouldCheckEmail,
      shouldResetPassword: shouldResetPassword ?? this.shouldResetPassword,
      defaultPassword: defaultPassword ?? this.defaultPassword,
      confirmPassword: confirmPassword ?? this.confirmPassword,
      authenticated: authenticated ?? this.authenticated,
      signedOut: signedOut ?? this.signedOut,
    );
  }
}

final authStateProvider = StateNotifierProvider<AuthNotifier>((ref) => AuthNotifier(AuthState.initial(), ref));



class AuthNotifier extends StateNotifier<AuthState> {

  final ProviderReference ref;

  AuthNotifier(AuthState state, this.ref) : super(state);

  checkEmail() async {
    bool? shouldResetPassword = await Services.api.get(Const.api.checkEmail + "/${state.email?.text.trim().toLowerCase()}");
    if (shouldResetPassword == null) return;

    state = state.copyWith(shouldCheckEmail: false, shouldResetPassword: shouldResetPassword);
  }

  signIn() async {
    String email = state.email!.text.toLowerCase();
    String password = state.password!.text.trim().toLowerCase();

    if (email.isEmpty || password.isEmpty) {
      Services.message.displayError("Tous les champs doivent être rempli");
      return;
    }

    Auth? auth = await Services.auth.signIn(email.trim(), password.trim());
    if (auth == null) return;

    ref.container.refresh(userFuture);
    ref.container.refresh(currentUserNotifier);
    state = state.copyWith(authenticated: true);
  }

  signOut() async {
    await Services.auth.signOut();
    state = state.copyWith(signedOut: true);
  }

  initialize() {
    state = AuthState.initial();
  }

  Future<void> resetPassword() async {
    String email = state.email!.text;
    String password = state.password!.text;
    String confirmPassword = state.confirmPassword!.text;
    String defaultPassword = state.defaultPassword!.text;

    if (email.isEmpty || defaultPassword.isEmpty) {
      Services.message.displayError("Tous les champs doivent être rempli");
      return;
    }

    if (password.compareTo(confirmPassword) != 0) {
      Services.message.displayError("Le nouveau mot de passe et sa confirmation ne correspondent pas");
      return;
    }

    Auth? auth = await Services.auth.resetPassword(email.trim(), defaultPassword.trim(), password.trim());
    if (auth == null) return;

    state = state.copyWith(authenticated: true);
  }
}