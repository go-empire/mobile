import 'package:empire/constants/constants.dart';
import 'package:empire/dto/create_file_dto.dart';
import 'package:empire/models/fichier.dart';
import 'package:empire/models/user.dart';
import 'package:empire/repository/repositories.dart';
import 'package:empire/services/services.dart';
import 'package:empire/states/base_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class UserState extends BaseState {
  User? user;
  List<String>? permissions;

  UserState({
    this.user,
    this.permissions,
  });

  UserState copyWith({
    User? user,
    List<String>? permissions,
  }) {
    return new UserState(
      user: user ?? this.user,
      permissions: permissions ?? this.permissions,
    );
  }
}

class UserListState extends BaseState {
  List<User> users;

  UserListState({
    required this.users,
  });

  UserListState copyWith({
    List<User>? users,
  }) {
    return UserListState(
      users: users ?? this.users,
    );
  }
}

final userFuture = FutureProvider<User>((ref) async {
  final data = await Services.api.get("${Const.api.currentUser}");
  print(data);
  return User.fromMap(data);
});

final userSelectedProvider = StateProvider<User?>((_) => null);
final userDetailsFutureProvider = FutureProvider.autoDispose.family<User?, String>((_, String id) async {
  return Repositories.user.get(id);
});

final currentUserNotifier = StateNotifierProvider<CurrentUserNotifier>((ref) {
  return CurrentUserNotifier(UserState()..loading());
});

final usersNotifier = StateNotifierProvider<UsersNotifier>((ref) {
  return UsersNotifier(UserListState(users: [])..loading(), ref);
});

class CurrentUserNotifier extends StateNotifier<UserState> {
  late User user;
  late List<String> permissions;

  CurrentUserNotifier(UserState state) : super(state) {
    _me();
    _permissions();
  }

  _permissions() async {
    final data = await Services.api.get("${Const.api.userPermissions}") as List;
    List<String> permissions = List.from(data);
    state = state.copyWith(permissions: permissions);
  }

  _me() async {
    try {
      final data = await Services.api.get("${Const.api.currentUser}");
      user = User.fromMap(data);
      state = state.copyWith(user: user);
    } catch(e) {
      state = state.copyWith()..error(e.toString());
    }
  }
}

class UsersNotifier extends StateNotifier<UserListState> {
  late User user;
  late List<String> permissions;
  ProviderReference ref;

  UsersNotifier(UserListState state, this.ref) : super(state) {
   _fetch();
  }

  _fetch() async {
    final users = await Repositories.user.fetch();
    if (users == null) return;

    state = state.copyWith(users: users);
  }



  Future<bool> update(String userId, Map<String, dynamic> payload, CreateFileDto? profile, List<CreateFileDto>? attachments) async {
    Fichier? file = profile != null ? await Services.api.uploadFile(profile.path, profile) : null;
    if (file != null) {
      payload["profilePictureId"] = file.id;
    }

    if (attachments != null) {
      attachments.forEach((attachment) async {
        attachment.owner = userId;
        await Services.api.uploadFile(attachment.path, attachment);
      });
    }

    User? user = await Repositories.user.update(userId, payload);
    if (user == null) return false;

    state = state.copyWith(users: state.users.map((u) => u.id == user.id ? user : u).toList());
    ref.read(userSelectedProvider).state = user;
    return true;
  }
}

