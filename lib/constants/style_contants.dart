import 'package:empire/constants/color_contants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StyleConstants {
  TextStyle bigTitle = TextStyle(fontSize: 50, color: Colors.white, fontWeight: FontWeight.w100);
  TextStyle normalText = TextStyle(fontSize: 16, color: Colors.white);
  TextStyle mediumText = TextStyle(fontSize: 16.sp, color: ColorConstants().primary);
  TextStyle mediumWhite = TextStyle(fontSize: 16.sp, color: Colors.white);
  TextStyle bodyText = TextStyle(fontSize: 14, color: ColorConstants().primary);
  TextStyle thinBodyText = TextStyle(fontSize: 12, color: ColorConstants().textBody, fontWeight: FontWeight.w300);
  TextStyle mediumBodyText = TextStyle(fontSize: 14, color: ColorConstants().primary, fontWeight: FontWeight.w600);
  TextStyle kpiLabel = TextStyle(fontSize: 28.sp, color: ColorConstants().textBody, fontWeight: FontWeight.w500);
  TextStyle kpiText = TextStyle(fontSize: 108.sp, fontWeight: FontWeight.bold);
  TextStyle buttonText = TextStyle(fontSize: 18.sp);
  TextStyle pageTitle = TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold, color: ColorConstants().primary);
}
