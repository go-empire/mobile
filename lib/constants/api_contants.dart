class ApiConstants {
  final baseUrl = "http://192.168.0.171:3000";
  // final baseUrl = "http://192.168.1.47:3000";
  // final baseUrl = "https://api.empire.awessome.fr";
  final uploadFile = "/file";
  final uploadFiles = "/files";
  final signIn = "/auth/signin";
  final signUp = "/auth/signup";
  final checkEmail = "/auth/check";
  final resetPassword = "/auth/reset";
  final refreshToken = "/auth/renew";
  final currentUser = "/users/me";
  final userPermissions = "/users/permissions";
  final adherents = "/adherents";
  final notifications = "/notifications";
  final categories = "/categories";
  final addPoints = "/adherents/add-points";
  final users = "/users";
  final conversations = "/chat/conversations";
  final chatMessages = "/chat/messages";
  final userConversation = "/chat/user";
  final allRepresentants = "/users/representants";
  final projects = "/projects";
  final articles = "/articles";
}