import 'package:flutter/material.dart';

class ColorConstants {
  Color background = Color(0xffF8F8F8);
  Color primary = Color(0xff4D4D4D);
  Color lightWhite = Color(0xfff8f8f8);
  Color darker = Color(0xff1c1c1c);
  Color secondary = Color(0xff363A3E);
  Color green = Color(0xff0FC949);
  Color red = Color(0xffF51C1C);
  Color blue = Color(0xff007AFF);
  Color orange = Color(0xffF5AA1C);
  Color yellow = Color(0xffDBDBAB);
  Color textBody = Color(0xffB6BCCC);
}