
import 'package:empire/constants/style_contants.dart';

import 'api_contants.dart';
import 'color_contants.dart';
import 'common_constant.dart';
import 'keys_constant.dart';
import 'shared_preferences_constant.dart';

export 'api_contants.dart';
export 'color_contants.dart';
export 'common_constant.dart';

class Const {
  static final ApiConstants api = ApiConstants();
  static final color = ColorConstants();
  static CommonConstant common = CommonConstant();
  static KeysConstant keys = KeysConstant();
  static SharedPreferencesConstants sp = SharedPreferencesConstants();
  static StyleConstants style = StyleConstants();
}
